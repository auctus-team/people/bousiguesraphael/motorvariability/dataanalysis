#----------------------------------------------------------------
# Generated CMake target import file for configuration "RelWithDebInfo".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "tropter" for configuration "RelWithDebInfo"
set_property(TARGET tropter APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(tropter PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libtropter.so"
  IMPORTED_SONAME_RELWITHDEBINFO "libtropter.so"
  )

list(APPEND _cmake_import_check_targets tropter )
list(APPEND _cmake_import_check_files_for_tropter "${_IMPORT_PREFIX}/lib/libtropter.so" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
