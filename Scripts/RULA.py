# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
# Name:        RULA.py
# Purpose:     Calcul de score RULA - mouvement de pointage
#
# Author:      jonathan.savin
#
# Created:     28/09/2018
# Copyright:   (c) jonathan.savin 2018
# Licence:     <your licence>
#
# Commentaire : validé à la main 20190829
#-------------------------------------------------------------------------------

import logging
import numpy  as np
import pandas as pd

__all__ = ['RULAprocessor']

# matrice 4*4 traduisant le tableau A de la méthode RULA
TABLE_A = np.array( [ [1,2,2,2,2,3,3,3],        # l.1
                      [2,2,2,2,3,3,3,3],
                      [2,3,3,3,3,3,4,4],
                      [2,3,3,3,3,4,4,4],
                      [3,3,3,3,3,4,4,4],        # l.5
                      [3,4,4,4,4,4,5,5],
                      [3,3,4,4,4,4,5,5],
                      [3,4,4,4,4,4,5,5],
                      [4,4,4,4,4,5,5,5],
                      [4,4,4,4,4,5,5,5],        # l.10
                      [4,4,4,4,4,5,5,5],
                      [4,4,4,5,5,5,6,6],
                      [5,5,5,5,5,6,6,7],
                      [5,6,6,6,6,7,7,7],
                      [6,6,6,7,7,7,7,8],        # l.15
                      [7,7,7,7,7,8,8,9],
                      [8,8,8,8,8,9,9,9],
                      [9,9,9,9,9,9,9,9] ] ).reshape([6,3,4,2])

TABLE_B = np.array( [ [1,3,2,3,3,4,5,5,6,6,7,7],        # l.1
                      [2,3,2,3,4,5,5,5,6,7,7,7],
                      [3,3,3,4,4,5,5,6,6,7,7,7],
                      [5,5,5,6,6,7,7,7,7,7,8,8],
                      [7,7,7,7,7,8,8,8,8,8,8,8],        # l.5
                      [8,8,8,8,8,8,8,9,9,9,9,9] ] ).reshape([6,6,2])

TABLE_C = np.array( [ [1,2,3,3,4,5,5],        # l.1
                      [2,2,3,4,4,5,5],
                      [3,3,3,4,4,5,6],
                      [3,3,3,4,5,6,6],
                      [4,4,4,5,6,7,7],       # l.5
                      [4,4,5,6,6,7,7],
                      [5,5,6,6,7,7,7],
                      [5,5,6,7,7,7,7] ] )

DEF_CLV_THRS = 1.
DEF_ABD_THRS = 10.  # up-arm abudction threshold, score A1
DEF_TRK_THRS = 10.  # trunk flexion threshold, score A1
DEF_AIR_THRS = 10.  # up-arm int/ext rotation, score A2
DEF_HND_FLX_THRS = 5. # hand flexion/extension thrshld, score A3
DEF_HND_ABD_THRS = 10. # hand flexion/extension thrshld, score A3
DEF_PRN_THRS = 5.  # fr-arm pronation, score A4

#===============================================================================
class RULAprocessor(object):
    """compute RULA scores based on postures, manipulated weight and task
    cycle time.
    """

    #---------------------------------------------------------------------------
    def __init__(self):
        self.postures = pd.Series()
        self.w = 0.
        self.task = { "isInterm"    : True,
                      "isStatic"      : False,
                      "isRepetv"     : False,
                      "withShck" : False}

    #---------------------------------------------------------------------------
    def setPosture(self,postr):
        """Initialize whole-body posture
        Input : postr, dic
        Values are joint angles, keyx are joint labels as defined in XDE
        """
        self.posture = postr

    #---------------------------------------------------------------------------
    def getPosture(self):
        """return the current whole-body posture
        """
        return self.posture

    #---------------------------------------------------------------------------
    def setTask(self, interm, stat, repet, shock):
        """
        """
        self.task["isInterm"] = interm
        self.task["isStatic"] = stat
        self.task["isRepetv"] = repet
        self.task["withShck"] = shock

    #---------------------------------------------------------------------------
    def setLoad(self,w):
        """Set the manipulated weight
        """
        self.w = w

    #---------------------------------------------------------------------------
    def getScoreA1(self, side="Right"):
        """Calcul du score A1 : bras
        Entrée : side (string) in ['Right','Left']
        Sortie : Entier entre 1 et 6
        """
        armFlx = self.posture[side+"Arm_Flx"]
        a1 = 0
        if np.abs(armFlx) < 20.:
            a1 = 1
        elif (armFlx < -20.) or ( (armFlx >= 20.) and (armFlx < 45.) ):
            a1 = 2
        elif (armFlx >= 45.) and (armFlx < 90.):
            a1 = 3
        else:
            a1 = 4
        if self.posture[side+"Clav_Elv"] > DEF_CLV_THRS:
            a1 += 1
        if self.posture[side+"Arm_Abd"] > DEF_ABD_THRS:
            a1 += 1
        if (self.posture["UpTrunk_Flx"] > DEF_TRK_THRS):
            a1 -= 1
        if a1 < 1:
            a1 = 1

##        logging.debug("Score A1=%d",a1)
        return a1

    #---------------------------------------------------------------------------
    def getScoreA2(self,side="Right"):
        """Calcul du score A2 : flex. avant-bras
        Entrée : side (string) in ['Right','Left']
        Sortie : Entier entre 1 et 3
        """
        elbFlx = self.posture[side+"ForeArm_Flx"]
        if (elbFlx < 60.) or (elbFlx >= 100.):
            a2 = 2
        else:
            a2 = 1
        if self.posture[side+"Arm_Rot"] < -DEF_AIR_THRS:  # théoriquement 0...
            a2 +=1
##TODO
#    if "main de l'autre côté du corps":
#    # critère cartésien : comment l'identifier en articulaire ?
#        a2 +=1

##        logging.debug("Score A2=%d",a2)
        return a2

    #---------------------------------------------------------------------------
    def getScoreA3(self,side="Right"):
        """Calcul du score A3 : poignet
        Entrée : side (string) in ['Right','Left']
        Sortie : Entier entre 1 et 4
        """
        a3 = 0
        wrFlx = self.posture[side+"Hand_Flx"]
        if np.abs(wrFlx) <= DEF_HND_FLX_THRS:     # théoriquement, que pour 0°...
            a3 = 1
        elif np.abs(wrFlx) <= 15.0:
            a3 = 2
        else:
            a3 = 3
        if np.abs(self.posture[side+"Hand_Abd"]) > DEF_HND_ABD_THRS:
            a3 += 1

##        logging.debug("Score A3=%d",a3)
        return a3

    #---------------------------------------------------------------------------
    def getScoreA4(self,side="Right"):
        """Calcul du score A4 : prono-supination
        Entrée : side (string) in ['Right','Left']
        Sortie : Entier entre 1 et 2
        """
        prnSup = self.posture[side+"ForeArm_Prn"] -90
            # /!\ : modèle XDE : ForeArm_Prn = 0 en position "Vitruve",

        a4 = 1
        if np.abs(prnSup) > DEF_PRN_THRS:     # théoriquement, que pour 0°...
            a4 = 2
##        logging.debug("Score A4=%d",a4)
        return a4

    #---------------------------------------------------------------------------
    def getScoreA5(self,side="Right"):
        """Calcul du score A5 :
        Entrée : side (string) in ['Right','Left']
        Sortie : Entier entre 1 et 9
        """
        scores = np.array([self.getScoreA1(side),
                           self.getScoreA2(side),
                           self.getScoreA3(side),
                           self.getScoreA4(side)]) - np.ones(4,dtype=int)
            # scores are in [1..N], python indices are [0..N-1]

        a5 = TABLE_A.item( *scores.tolist() )
##        logging.debug("Score A5=%d",a5)
        return a5

    #---------------------------------------------------------------------------
    def getScoreA6(self):
        """Calcul du score A6 : muscles
        Sortie : Entier entre 0 et 1
        """
        if self.task["isStatic"] or self.task["isRepetv"]:
            a6 = 1
        else:
            a6 = 0

##        logging.debug("Score A6=%d",a6)
        return a6

    #---------------------------------------------------------------------------
    def getScoreA7(self):
        """Calcul du score A7 : force
        Sortie : Entier entre 0 et 3
        """
        a7 = 0

        if self.task["isInterm"] and (self.w >= 2.) and (self.w < 10.):
            a7 = 1
        if (self.task["isStatic"] or self.task["isRepetv"]):
            if (self.w >= 2.) and (self.w < 10.):
                a7 = 2
        if (self.w >= 10.) and (self.task["isStatic"] or self.task["isRepetv"]):
            a7 = 3
##        logging.debug("Score A7=%d",a7)
        return a7

    #---------------------------------------------------------------------------
    def getAScores(self):
        """Calcul des scores A1 à A8 :
        Sortie : liste de 8 entiers
        """
        return [ self.getScoreA1(),
                 self.getScoreA2(),
                 self.getScoreA3(),
                 self.getScoreA4(),
                 self.getScoreA5(),
                 self.getScoreA6(),
                 self.getScoreA7(),
                 self.getScoreC() ]

    #---------------------------------------------------------------------------
    def getBScores(self):
        """Calcul des scores B1 à B7 :
        Sortie : liste de 7 entiers
        """
        return [ self.getScoreB1(),
                 self.getScoreB2(),
                 self.getScoreB3(),
                 self.getScoreB4(),
                 self.getScoreB5(),
                 self.getScoreB6(),
                 self.getScoreD() ]

    #---------------------------------------------------------------------------
    def getScoreC(self,side="Right"):
        """Calcul du score C
        Sortie : Entier entre 1 et 13
        """
        c = self.getScoreA5(side) + self.getScoreA6() + self.getScoreA7()
##        logging.debug("Score C = %d",c)
        return c

    #---------------------------------------------------------------------------
    def getScoreB1(self):
        """Calcul du score B1 : tête
        Sortie : Entier entre 1 et 5
        /!\ TODO
        """
        hdFlx = self.posture["Head_Flx"]
        hdLat = self.posture["Head_Lat"]
        hdRot = self.posture["Head_Rot"]
        if (hdFlx >=0) and (hdFlx <10):
            b1 = 1
        if (hdFlx >=10) and (hdFlx <20):
            b1 = 2
        if hdFlx >=20:
            b1 = 3
        if hdFlx <0:
            b1 = 4
        if np.abs(hdRot) > 10:
            b1 += 1
        if np.abs(hdLat) > 10:
            b1 += 1
        return b1

    #---------------------------------------------------------------------------
    def getScoreB2(self):
        """Calcul du score B2 : Tronc
        Sortie : Entier entre 1 et 6
        /!\ TODO
        """
        b2 = 1
        flx = self.posture["UpTrunk_Flx"]
        lat = self.posture["UpTrunk_Lat"]
        rot = self.posture["UpTrunk_Rot"]
        if (flx >= 5.) and (flx < 20.):
            b2 = 2
        if (flx >= 20.) and (flx < 60.):
            b2 = 3
        if (flx >= 60.):
            b2 = 4
        if np.abs(lat) > 10:
            b2 += 1
        if np.abs(rot) > 10:
            b2 += 1
        return b2

    #---------------------------------------------------------------------------
    def getScoreB3(self):
        """Calcul du score B3 : jambes
        Sortie : Entier entre 1 et 2
        /!\ TODO
        """
        try:
            b3 = 1 if self.posture["Legs"] == 2 else 2
        except KeyError:
##            logging.info("No info for legs at ground, assuming bipede station")
            b3 = 1
        return b3

    #---------------------------------------------------------------------------
    def getScoreB4(self):
        """Calcul du score B4 :
        Sortie : Entier entre 1 et 9
        /!\ TODO
        """
        scores = np.array([self.getScoreB1(),
                           self.getScoreB2(),
                           self.getScoreB3()]) - np.ones(3,dtype=int)
                    # scores are in [1..N], python indices are [0..N-1]
        b4 = TABLE_B.item( *scores.tolist() )
##        logging.debug("Score B4=%d",b4)
        return b4

    #---------------------------------------------------------------------------
    def getScoreB5(self):
        """Calcul du score B5 : muscles
        """
        b5 = self.getScoreA6()
##        logging.debug("Score B5=%d",b5)
        return b5

    #---------------------------------------------------------------------------
    def getScoreB6(self):
        """Calcul du score B6 : force
        """
        b6 = self.getScoreA7()
##        logging.debug("Score B6=%d",b6)
        return b6

    #---------------------------------------------------------------------------
    def getScoreD(self):
        """Calcul du score D :
        """
        d = self.getScoreB4() + self.getScoreB5() + self.getScoreB6()
##        logging.debug("Score D = %d",d)
        return d

    #---------------------------------------------------------------------------
    def getFinalScore(self,side="Right"):
        """Calcul du score final issu des scores C et D
        """
        c = self.getScoreC(side)
        if c >= 8:
            c = 8
        d = self.getScoreD()
        if d >= 7:
            d = 7
            # d -= 1 # i don't know from where it comes

        fs = TABLE_C[c-1,d-1]   # scores are in [1..N], python indices are [0..N-1]
##        logging.debug("Score Final =%d",fs)
        return fs
    
    def getFinalScoreUpperBodyOnly(self,side="Right"): # Considers that the lower body, the trunc and the head are in ideal conditions
        """Calcul du score final issu des scores C et D
        """
        c = self.getScoreC(side)
        if c >= 8:
            c = 8
        d = 1

        fs = TABLE_C[c-1,d-1]   # scores are in [1..N], python indices are [0..N-1]
##        logging.debug("Score Final =%d",fs)
        return fs

#===============================================================================
def main():

    logging.debug("Running demo test...")

    # Préparation des données test
    post_dict_A = {
                # step        1   2  3  4  5  6  7   8  9 10 11 12 13 14 15 16  17
        "RightArm_Flx"    :[-30,-10,10,10,10,10,10, 10,10,10,10,10,10,10,30,60,100],
        "RightClav_Elv"   :[  0,  0, 0,10, 0, 0, 0,  0, 0, 0, 0, 0, 0, 0, 0, 0,  0],
        "RightArm_Abd"    :[  0,  0, 0, 0,15, 0, 0,  0, 0, 0, 0, 0, 0, 0, 0, 0,  0],
        "UpTrunk_Flx"     :[  0,  0, 0, 0, 0,15, 0,  0, 0, 0, 0, 0, 0, 0, 0, 0,  0],
        "RightForeArm_Flx":[ 90, 90,90,90,90,90,30,120,30,90,90,90,90,90,90,90, 90],
        "RightArm_Rot"    :[  0,  0, 0, 0, 0, 0, 0,  0,-15,0, 0, 0, 0, 0, 0, 0, 0],
        "RightHand_Flx"   :[  0,  0, 0, 0, 0, 0, 0,  0, 0, 0,10,25, 0, 0, 0, 0,  0],
        "RightHand_Abd"   :[  0,  0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0,15, 0, 0, 0,  0],
        "RightForeArm_Prn":[ 90, 90,90,90,90,90,90, 90,90,90,90,90,90,70,90,90, 90] }

    post_dict_B = {
                # step        1  2  3  4   5  6  7  8  9 10 11 12 13
        "Head_Flx"        :[  5,15,25,70,-20, 5, 5, 5, 5, 5, 5, 5, 5],
        "Head_Lat"        :[  0, 0, 0, 0,  0,20, 0, 0, 0, 0, 0, 0, 0],
        "Head_Rot"        :[  0, 0, 0, 0,  0, 0,20, 0, 0, 0, 0, 0, 0],
        "UpTrunk_Flx"     :[  0, 0, 0, 0,  0, 0, 0,10,30,70, 0, 0, 0],
        "UpTrunk_Lat"     :[  0, 0, 0, 0,  0, 0, 0, 0, 0, 0,20, 0, 0],
        "UpTrunk_Rot"     :[  0, 0, 0, 0,  0,15, 0, 0, 0, 0, 0,20, 0],
        "Legs"            :[  2, 2, 2, 2,  2, 2, 2, 2, 2, 2, 2, 2, 1] }

    # initialisations
    dfA = pd.DataFrame(data=post_dict_A)
    dfB = pd.DataFrame(data=post_dict_B)

    rp = RULAprocessor()
    rp.setTask(False,False,True,False)    # interm, stat, repet, shock
    rp.setLoad(0.)

    import matplotlib.pyplot as plt

    # Évaluation RULA scores A
##    stpNb = dfA.shape[0] # nb de steps (values)
##    for i in range(stpNb):
##        pstA = dfA[:].iloc[i]
##        rp.setPosture(pstA)
##        A_scores = rp.getAScores()
##
##        f = plt.figure()
##
##        f.suptitle("Scores A - step %d"%(i+1))
##
##        ax=f.add_subplot(211)
##        ax.set_title("Scores A RULA")
##        ax.grid("on")
##        ax.bar(np.arange(8)+1,A_scores)
##
##        # angles
##        ax=f.add_subplot(212)
##        ax.set_title("Angles")
##        ax.grid("on")
##        plt.bar(np.arange(len(pstA)),pstA)
##        plt.xticks(np.arange(len(pstA)),pstA.index,rotation=45)
##
##        plt.show()

    # Évaluation RULA scores B
    stpNb = dfB.shape[0] # nb de steps (values)
    for i in range(stpNb):
        pstB = dfB[:].iloc[i]
        rp.setPosture(pstB)

        B_scores = rp.getBScores()

        f = plt.figure()

        f.suptitle("Scores B - step %d"%(i+1))

        ax=f.add_subplot(211)
        ax.set_title("Scores B RULA")
        ax.grid("on")
        ax.bar(np.arange(7)+1,B_scores)

        # angles
        ax=f.add_subplot(212)
        ax.set_title("Angles")
        ax.grid("on")
        plt.bar(np.arange(len(pstB)),pstB)
        plt.xticks(np.arange(len(pstB)),pstB.index,rotation=45)

        plt.show()

    logging.debug("Done !")

#===============================================================================
if __name__ == '__main__':
	logging.basicConfig(level=logging.DEBUG,
						format='%(levelname)s\t%(module)s::%(funcName)s (%(lineno)d): %(message)s',
						datefmt='%H:%M:%S')
	main()
