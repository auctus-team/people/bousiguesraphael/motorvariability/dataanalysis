campaign_name="Mover202207"
# subject_list=("6726" "6843" "7961")
subject_list=("1543" "2682" "3549" "4734" "5165" "5724" "6726" "6843" "7961")
# subject_list=("1543" "8947" "7961" "2682" "3549" "5724" "6843" "6726" "9536" "4734" "5165")
# file_prefix="Mover20220728_$subject""_Xsens_0_"
# file_suffix="-001"

echo 'Default paths for data are set by the folder: /home/raphael/Documents/MOVER/Data/'$campaign_name
for subject in ${subject_list[@]}
do
	for trial in {1..50}
	do
		# python3.9 GapFiller.py $campaign_name $subject $trial 
		# python3.9 PostGapFilling.py $campaign_name $subject $trial
		# python3.9 QualisysFilter.py $campaign_name $subject $trial 
		python3.9 Ergonomics.py $campaign_name $subject $trial 
	done
done
