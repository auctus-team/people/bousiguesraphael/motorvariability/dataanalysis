##################################################################################
# Libraries
from cProfile import label
from doctest import testmod
import opensim as osim
from pyosim.OsimModel import OsimModel 
import scaling_test

import csv
import numpy as np

import matplotlib.pyplot as plt

from mpl_toolkits import mplot3d

##################################################################################
# Code from Pauline Maurice work, not used in MOVER n°1
##################################################################################

##################################################################################
## Files
# Input files
model_path = '/media/raphael/RaphaelUSBB/Rajagopal2015-MOVER.osim'
mocap_filename = '/media/raphael/RaphaelUSBB/0001bis.trc'
scale_setup_filename = '/home/raphael/Documents/app/dataanalysis/Data/OSIM_Scale_1543.xml'

marker_placement_file = 'MarkerPlacement.csv'
markerList =[]

# Output files
scaled_model_filename = 'Rajagopal2015-MOVER_SCALED.osim'
adjusted_model_filename = 'Rajagopal2015-MOVER_ADJUSTED.osim'


##################################################################################
## Computing
# Define Model
my_model = osim.Model(model_path)
state = my_model.initSystem()

# Get trc data to determine time range
markerData = osim.MarkerData(mocap_filename)
initial_scale_time = markerData.getStartFrameTime()
final_scale_time = markerData.getLastFrameTime()
timeArray = osim.ArrayDouble()
timeArray.set(0,initial_scale_time)
timeArray.set(1,final_scale_time)




markerSet=my_model.getMarkerSet()
for marker in markerSet:
    marker_name = marker.getName()
    # print(marker_name)
    markerList.append(marker_name)
print("Marker List= ", markerList)



## Create scale tool
scaleTool = osim.ScaleTool(scale_setup_filename)


# Tell scale tool to use the loaded model
scaleTool.getGenericModelMaker().setModelFileName(model_path)

## ModelScaler-
# Whether or not to use the model scaler during scale
scaleTool.getModelScaler().setApply(True)
# Set the marker file (.trc) to be used for scaling 
scaleTool.getModelScaler().setMarkerFileName(mocap_filename)
# set a time range
scaleTool.getModelScaler().setTimeRange(timeArray)
# Indicating whether or not to preserve relative mass between segments
scaleTool.getModelScaler().setPreserveMassDist(False)
#Name of OpenSim model file (.osim) to write when done scaling.
scaleTool.getModelScaler().setOutputModelFileName(scaled_model_filename)
# Run model scaler Tool
scaleTool.getModelScaler().processModel(my_model, scaleTool.getPathToSubject())

# Update state to current state
state = my_model.getWorkingState()

## ModelPlacer-
# Whether or not to use the model scaler during scale
scaleTool.getMarkerPlacer().setApply(True)
# Set the marker placer time range
scaleTool.getMarkerPlacer().setTimeRange(timeArray)
# Set the marker file (.trc) to be used for scaling 
scaleTool.getMarkerPlacer().setStaticPoseFileName(mocap_filename)
scaleTool.getMarkerPlacer().setOutputModelFileName(adjusted_model_filename)
# Maximum amount of movement allowed in marker data when averaging
scaleTool.getMarkerPlacer().setMaxMarkerMovement(-1)
# Run Marker Placer
scaleTool.getMarkerPlacer().processModel(my_model, '')

# Update state to current state
state = my_model.getWorkingState()

with open(marker_placement_file, 'a') as fout:
    writer = csv.writer(fout, delimiter = ',')
    data_marker = []
    for m in markerList:
        for j in range(3):
            data_marker.append(my_model.getMarkerSet().get(m).get_location().get(j))
    writer.writerow(data_marker)

# Lock torso displacements
for coord in ['r_x', 'r_y', 'r_z', 't_x', 't_y', 't_z']:
    c = my_model.getCoordinateSet().get(coord)
    c.set_default_value(c.getValue(state))
    c.set_locked(1)
