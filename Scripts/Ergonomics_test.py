import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from Twist import TwistAngle
sns.set_theme()

from mpl_toolkits import mplot3d 
from mpl_toolkits.mplot3d import Axes3D

import os, sys

# Import MOVER functions
import resample
import IK
import JointsCenters
import Swivel
import Twist
import S
import RULA_mover

##########################################
# IMPORTANT Prequisites
##########################################
# Please, run JointsCenters.py first !
print('Did you run GapFiller.py first ?\n')
##########################################

####################################################
# Useful functions
def Distance(x1,y1,z1,x2,y2,z2):
    return np.sqrt((x1-x2)**2 + (y1-y2)**2 + (z1-z2)**2 )

# In[]
#####################################################

# Cheat Part: juste to debug this programm as a standalone 
# Define subject and trial to compute
campaign_name = 'Mover202207'
subject = '1543'
trial = '0001'
sys.argv.append(campaign_name)
sys.argv.append(subject)
sys.argv.append(int(trial))

input_file = ""

# Listen .sh parameters
if(len(sys.argv) == 1):
    print("Please provide the campaign name, the subject name and the trial")
    exit()
elif(len(sys.argv) == 2):
    print("Please provide the subject name and the trial")
    exit()
elif(len(sys.argv) == 3):
    print("Please provide the trial")
    exit()
else:
    campaign_name = sys.argv[1]
    subject = sys.argv[2]
    trial = int(sys.argv[3])
    trial_str = f'{trial:04d}'

##################################################################################
## Folders
campaign_folder = '/home/raphael/Documents/MOVER/Data/'+campaign_name
qualisys_folder = campaign_folder+'/Qualisys/'
opensim_folder = campaign_folder+'/OpenSIM/'
ros_folder = campaign_folder+'/ROS/'
rt_folder = campaign_folder+'/RT/'

##################################################################################
# Verify existence of destination folders ==> Campaign and Qualisys MUST exist !
if not os.path.isdir(opensim_folder):
    os.makedirs(opensim_folder)

if not os.path.isdir(rt_folder):
    os.makedirs(rt_folder)

# ##################################################################################
# # First: run IK
# # Input: .trc file, .xml file, .osim scaled model
# # Output: .mot file

# # Input files
# model_path = opensim_folder+subject+'/Rajagopal2015-MOVER_'+subject+'.osim'
# IK_setup_file_path = opensim_folder+subject+'/OSIM_'+subject+'.xml'
# input_mocap_file_path = qualisys_folder+subject+'/'+trial_str+'_full_clean_filtered.trc' # from GapFiller.py only and then filtered with QualisysFilter.py => prequisite

# # Output files
# mot_file_path = opensim_folder+subject+'/IK_out_'+trial_str+'.mot'

# # Call function
# IK.IK_osim(model_path,input_mocap_file_path,IK_setup_file_path,mot_file_path)



##################################################################################
# Third_D: Compute RULA
# Input: .csv file
# Output: Pandas DataFrame

# Input file
mot_file_path = opensim_folder+subject+'/IK_out_'+trial_str+'.mot'

# Ouput file
rula_file_path = rt_folder+subject+'/'+trial_str+'_RULA.csv'

print("\n######\nComputing RULA...\n")

# Call function
rula_df = RULA_mover.RULADataFrame(mot_file_path)
# print("rula_df=\n",rula_df)





