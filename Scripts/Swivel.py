
import numpy as np
import pandas as pd

from scipy import linalg
from spatialmath import *
from spatialmath.base import trnorm

#####################################################
def Distance(A,B):
    d = np.sqrt((A[0]-B[0])**2 + (A[1]-B[1])**2 + (A[2]-B[2])**2)
    return d

############################################################################################
############################################################################################
# define the Twist function which return twist angle
def SwivelDataFrame(markers_csv_path,swivel_file_path):

    ###############################################
    # Import Data
    data = pd.read_csv(markers_csv_path)

    ###############################################
    print('Computing V vector')
    V = pd.DataFrame({'V X':(data['WRI_C_x']-data['SHO_C_x']),
                'V Y':(data['WRI_C_y']-data['SHO_C_y']),
                'V Z':(data['WRI_C_z']-data['SHO_C_z'])})
    normV = pd.DataFrame({'normV':np.sqrt(V['V X']**2+V['V Y']**2+V['V Z']**2)})
    V = -pd.DataFrame({'V X':V['V X']/normV['normV'],
                    'V Y':V['V Y']/normV['normV'],
                    'V Z':V['V Z']/normV['normV']})  #### !! with minus, V is defined here, from wrist to shoulder : V = - pd.dataframe...


    ##############################################                  
    print("Computing W vector")
    ref=[0,0,-1] # reference axis along downward Z-axis
    Wz0 = pd.DataFrame({'Wz0':ref[0]*V['V X']+ref[1]*V['V Y']+ref[2]*V['V Z']})
    W = pd.DataFrame({'W X':ref[0]-Wz0['Wz0']*V['V X'],
                    'W Y':ref[1]-Wz0['Wz0']*V['V Y'],
                    'W Z':ref[2]-Wz0['Wz0']*V['V Z']})
    normW = pd.DataFrame({'normW':np.sqrt(W['W X']**2+W['W Y']**2+W['W Z']**2)})
    W = pd.DataFrame({'W X':W['W X']/normW['normW'],
                    'W Y':W['W Y']/normW['normW'],
                    'W Z':W['W Z']/normW['normW']})

    ##############################################
    print('Computing U vector')
    U = pd.DataFrame({'U X':(V['V Y']*W['W Z']-V['V Z']*W['W Y']),
                    'U Y':(V['V Z']*W['W X']-V['V X']*W['W Z']),
                    'U Z':(V['V X']*W['W Y']-V['V Y']*W['W X'])})
    normU = pd.DataFrame({'normU':np.sqrt(U['U X']**2+U['U Y']**2+U['U Z']**2)})



    ##############################################
    print('Computing Shoulder to Elbow norm')
    # Compute Shoulder to Elbow norm
    SE = pd.DataFrame({'SE X':(data['ELB_C_x']-data['SHO_C_x']),
                    'SE Y':(data['ELB_C_y']-data['SHO_C_y']),
                    'SE Z':(data['ELB_C_z']-data['SHO_C_z'])})
    normSE = pd.DataFrame({'normSE':np.sqrt(SE['SE X']**2+SE['SE Y']**2+SE['SE Z']**2)})

    ##############################################
    print('Computing Elbow to Wrist norm')
    # Compute Elbow to Wrist norm
    EW = pd.DataFrame({'EW X':(data['WRI_C_x']-data['ELB_C_x']),
                    'EW Y':(data['WRI_C_y']-data['ELB_C_y']),
                    'EW Z':(data['WRI_C_z']-data['ELB_C_z'])})
    normEW = pd.DataFrame({'normEW':np.sqrt(EW['EW X']**2+EW['EW Y']**2+EW['EW Z']**2)})
    #print("Mon essai")
    print(normEW.mean())
    # print(data)
    # print(data.iloc[:,3])
    # 
    # ##############################################

    Ones=pd.DataFrame(np.ones(data.shape[0]),columns=['Ones']) # Dataframe of same size as Data, full of ones

    l_U = normSE.mean().values[0]; # 0.28 ; # upper arm size (m)
    l_F = normEW.mean().values[0]; # 0.26 ; # forearm size (m)

    ##############################################

    print('Computing Shoulder to Wrist norm')
    # Compute Shoulder to Wrist norm
    SW = pd.DataFrame({'SW X':(data['WRI_C_x']-data['SHO_C_x']),
                    'SW Y':(data['WRI_C_y']-data['SHO_C_y']),
                    'SW Z':(data['WRI_C_z']-data['SHO_C_z'])})
    normSW = pd.DataFrame({'normSW':np.sqrt(SW['SW X']**2+SW['SW Y']**2+SW['SW Z']**2)})
    # Compute cos(alpha) value
    cos_alpha = pd.DataFrame({'cos_alpha':(normSW['normSW']**2 + Ones['Ones']*(l_U**2 - l_F**2))/(2 * Ones['Ones'] * l_U * normSW['normSW'])})

    ##############################################

    print('Computing Swivel Angle Rotation Center point')
    # Compute Swivel Angle Rotation Center point
    PC = pd.DataFrame({'PC X':(data['SHO_C_x'] + l_U * cos_alpha['cos_alpha'] * V['V X']),
                    'PC Y':(data['SHO_C_y'] + l_U * cos_alpha['cos_alpha'] * V['V Y']),
                    'PC Z':(data['SHO_C_z'] + l_U * cos_alpha['cos_alpha'] * V['V Z'])})

    ##############################################

    print('Computing Center to Elbow vector')
    # Compute Center to Elbow vector
    CE = pd.DataFrame({'CE X':(data['ELB_C_x']-PC['PC X']),
                    'CE Y':(data['ELB_C_y']-PC['PC Y']),
                    'CE Z':(data['ELB_C_z']-PC['PC Z'])})

    print('Computing projection of CE on U vector')
    # Compute projection of CE on U vector
    CE_U = pd.DataFrame({'CE_U':(CE['CE X'] * U['U X'] +
                                CE['CE Y'] * U['U Y'] +
                                CE['CE Z'] * U['U Z'] )})
    print('Computing projection of CE on W vector')
    # Compute projection of CE on W vector
    CE_W = pd.DataFrame({'CE_W':(CE['CE X'] * W['W X'] +
                                CE['CE Y'] * W['W Y'] +
                                CE['CE Z'] * W['W Z'] )})
    
    #####################################################
    print(' -- Computing Swivel Angle')
    # Compute Swivel Angle
    trial_swivel_df = pd.DataFrame({'Time':data['Time'],'SwivelAngle':(np.arctan2(CE_U['CE_U'] , CE_W['CE_W']))})
    # trial_swivel_df = pd.DataFrame({'Time':(data['timestamp']-data['timestamp'][0])/1000,'SwivelAngle':(180/np.pi * np.arctan2(CE_U['CE_U'] , CE_W['CE_W'])),'Trial':np.ones(np.size(data['timestamp']))*trial})
    # print(trial_swivel_df)
    # swivel_df = swivel_df.append(trial_swivel_df, ignore_index = True)
    # print(swivel_df)

    ########################################
    # Export Data
    print('Exporting Data file...')
    trial_swivel_df.to_csv(swivel_file_path)
    print('\t- Data exported to:',swivel_file_path)

    ########################################
    # Return Data
    return trial_swivel_df




    # # UNCOMMENT TO RECOMPUTE
    # for trial in range (trial_start,trial_end+1):
    #     try:

    #         title = "Subject n°"+str(subject_name)+" - Trial n°"+str(trial)
    #         # Prepare file path
    #         # file_q = f'{trial:04d}.tsv' # To keep 4 digits number
    #         import_file_q = f'Qualisys_{trial:04d}.csv'
    #         import_file_r = f'ROS_{trial:04d}_sync.csv'
    #         import_file_x = 'Xsens_'+str(trial)+'.csv'
    #         export_file = f'RT_{trial}_test.csv'

    #         qualisys_path = campaign_name+'/Qualisys/'+subject_name+'/'+import_file_q
    #         ros_path = campaign_name+'/ROS/'+subject_name+'/'+import_file_r
    #         xsens_path = campaign_name+'/Xsens/'+subject_name+'/'+import_file_x

    #         swivel_file_path = export_folder + export_file

    #         print(qualisys_path)
    #         print(ros_path)
    #         print(xsens_path)

    #         # Load dataframes
    #         # data_q = pd.read_csv(qualisys_path,  delimiter = ',') # useless in a first time
    #         print("Import ROS Data")
    #         data_r = pd.read_csv(ros_path,  delimiter = ',')
    #         print("\t- ROS Data imported")
    #         print("Import Xsens Data")
    #         data_x = pd.read_csv(xsens_path,  delimiter = ',')
    #         print("\t- Xsens Data imported")

    #         # Keep only usefull columns
    #         data_r=data_r[['Time','s','TwistAngle',curr_pose_x,curr_pose_y,curr_pose_z]]
    #         data_x=data_x[['Time','SwivelAngle']]
    #         print('data_r =',data_r)
    #         print('data_x =',data_x)

        
    #         # Rounding time values to the millisecond
    #         # data_q['Time'] = data_q['Time'].round(0)# useless in a first time
    #         # data_r['Time'] = data_r['Time'].round(0)
    #         data_x['Time'] = data_x['Time'].round(0)

    #         # Resample ROS data to xsens_frequency
    #         data_r_old = data_r
    #         data_r = Resample(data_r,'Time',new_time_col=data_x['Time'])
    #         # data_r = Resample(data_r,'Time')

    #         # Show resampled data for ROS
    #         fig1 = plt.figure(figsize=(20,20))
    #         ax1 = fig1.add_subplot(111)
    #         ax1.set_title(title)
    #         ax1.set_ylabel("Time (s)")
    #         ax1.set_xlabel("s")
    #         ax1.set_xlim(auto=True)
    #         ax1.set_ylim(auto=True)
    #         ax1.plot(data_r_old['Time'],data_r_old['s'],label='Old S', color='red')
    #         ax1.plot(data_r['Time'],data_r['s'],'b+',label='New S')
    #         ax1.legend()
    #         # plt.show()


    #         # In[]
    #         #####################################################
    #         # Combining Qualisys, Xsens and ROS Values on the same rows, with the corresponding time stamp
    #         combine_df = data_x.set_index('Time').combine_first(data_r.set_index('Time')).reset_index()
    #         # combine_df = combine_df.set_index('Time').combine_first(data_q.set_index('Time')).reset_index() # useless in a first time

    #         ### Cleaning NaN rows
    #         for col in combine_df.columns:
    #             combine_df = combine_df[combine_df[col].notnull()]
    #         combine_df = combine_df.reset_index()

    #         # In[]
    #         #####################################################
    #         # Write Time as seconds from the beginning, and timestamp in seconds
    #         print("Correcting Time")
    #         print("\t-Set Timestamp in seconds")
    #         combine_df['timestamp'] = combine_df['Time']/1000
    #         print("\t-Set Time in seconds from the beginning of the trial")
    #         combine_df['Time'] = (combine_df['Time'] - combine_df['Time'][0])/1000

    #         # In[]
    #         #####################################################
    #         # Convert TwistAngle column from radians to degrees
    #         combine_df['TwistAngle'] = combine_df['TwistAngle'] * 180 / np.pi

    #         # In[]
    #         #####################################################
    #         # Compute Handle center velocity
    #         combine_df['dt']=combine_df['Time'].diff() # dt is expressed in seconds

    #         combine_df['dx']=combine_df[curr_pose_x].diff()
    #         combine_df['dy']=combine_df[curr_pose_y].diff()
    #         combine_df['dz']=combine_df[curr_pose_z].diff()

    #         combine_df['Vel'] = np.sqrt(combine_df['dx']**2 + combine_df['dy']**2 +combine_df['dz']**2 ) / combine_df['dt']

    #         fig2 = plt.figure(figsize=(20,20))
    #         ax21 = fig2.add_subplot(211)
    #         ax21.set_title(title)
    #         ax21.set_ylabel("Vel (m/s)")
    #         ax21.set_xlabel("s")
    #         ax21.set_xlim(auto=True)
    #         ax21.set_ylim(auto=True)
    #         ax21.plot(combine_df['s'],combine_df['Vel'],label='Handle Velocity', color='red')
    #         ax21.legend()
    #         ax22 = fig2.add_subplot(212)
    #         ax22.set_ylabel("Vel (m/s)")
    #         ax22.set_xlabel("Time (s)")
    #         ax22.set_xlim(auto=True)
    #         ax22.set_ylim(auto=True)
    #         ax22.plot(combine_df['Time'],combine_df['Vel'],label='Handle Velocity', color='red')
    #         ax22.legend()
    #         # plt.show()

    #         # In[]
    #         #####################################################
    #         # Compute Handle center Acceleration

    #         combine_df['ddx']=combine_df['dx'].diff()
    #         combine_df['ddy']=combine_df['dy'].diff()
    #         combine_df['ddz']=combine_df['dz'].diff()

    #         combine_df['Acc'] = np.sqrt(combine_df['ddx']**2 + combine_df['ddy']**2 +combine_df['ddz']**2 ) / combine_df['dt']

    #         fig3 = plt.figure(figsize=(20,20))
    #         ax31 = fig3.add_subplot(211)
    #         ax31.set_title(title)
    #         ax31.set_ylabel("Acc (m/s^2)")
    #         ax31.set_xlabel("s")
    #         ax31.set_xlim(auto=True)
    #         ax31.set_ylim(auto=True)
    #         ax31.plot(combine_df['s'],combine_df['Acc'],label='Handle Acceleration', color='green')
    #         ax31.legend()
    #         ax32 = fig3.add_subplot(212)
    #         ax32.set_ylabel("Acc (m/s^2)")
    #         ax32.set_xlabel("Time (s)")
    #         ax32.set_xlim(auto=True)
    #         ax32.set_ylim(auto=True)
    #         ax32.plot(combine_df['Time'],combine_df['Acc'],label='Handle Acceleration', color='green')
    #         ax32.legend()
    #         # plt.show()

    #             # In[]
    #         #####################################################
    #         # Compute Advance directive

    #         combine_df['s_c']=combine_df['s']*0 + s_min

    #         idx_min = 0
    #         for row in range(combine_df.shape[0]):
    #             if combine_df['s'][row] >= s_min and idx_min == 0:
    #                 idx_min = row
    #             if idx_min!=0:
    #                 combine_df['s_c'][row] = min(combine_df['s_c'][idx_min] + (combine_df['Time'][row]-combine_df['Time'][idx_min]) * s_c_speed / L_wireloop, 1.0)

    #         fig4 = plt.figure(figsize=(20,20))
    #         ax41 = fig4.add_subplot(111)
    #         ax41.set_title(title)
    #         ax41.set_ylabel("s")
    #         ax41.set_xlabel("Time (s)")
    #         ax41.set_xlim(auto=True)
    #         ax41.set_ylim(auto=True)
    #         ax41.plot(combine_df['Time'],combine_df['s_c'],label='Advance Directive', color='red')
    #         ax41.plot(combine_df['Time'],combine_df['s'],label='Real Advance', color='blue')
    #         ax41.legend()
    #         # plt.show()


    #         #####################################################
    #         # Create a specific dataframe for Redundancy Tubes and re-organize columns
    #         trial_rt_df = combine_df[['Time','timestamp','s','s_c','SwivelAngle','TwistAngle','Vel','Acc']]
    #         print('trial_rt_df :',trial_rt_df)
    #         # trial_rt_df.to_csv(swivel_file_path, index=False) # Data with NaN values !
    #         # Be careful ! Data don't always fall on the same timestamp !

    #         #####################################################
    #         # Clean lines with NaN values # USELESS BECAUSE ALREADY DONE ABOVE
    #         # trial_rt_df = trial_rt_df[trial_rt_df['s'].notnull()]
    #         # trial_rt_df = trial_rt_df[trial_rt_df['SwivelAngle'].notnull()]
    #         # trial_rt_df = trial_rt_df[trial_rt_df['TwistAngle'].notnull()]
    #         # trial_rt_df.reset_index()

    #         #####################################################
    #         # Memorize trial
    #         trial_rt_df['Trial'] = np.ones(trial_rt_df.shape[0]) * trial
    #         trial_rt_df = trial_rt_df[3:]

    #         # Avoid side-effects 
    #         trial_rt_df = trial_rt_df[trial_rt_df['s']>=s_min]
    #         trial_rt_df = trial_rt_df[trial_rt_df['s']<=s_max]
    #         #####################################################
    #         # Export

    #         print('trial_rt_df when exporting:',trial_rt_df)
    #         trial_rt_df.to_csv(swivel_file_path, index=False)


    #         #####################################################
    #         # Stacking on a bigger RT dataframe

    #         rt_df = rt_df.append(trial_rt_df, ignore_index = True)


    #     except:
    #         print('Trial n°',trial,' not found')

    # rt_df.to_csv(export_folder + 'RT_'+str(trial_start)+'-'+str(trial_end)+'.csv', index=False)

    # rt_df = pd.read_csv(export_folder + 'RT_'+str(trial_start)+'-'+str(trial_end)+'.csv',  delimiter = ',')
    # rt_df['Trial']=rt_df['Trial'].astype(int)
    # print("RT=\n",rt_df)
    # print('Dataframe loaded')

    # # dataframe for trial n°10 only:
    # rt_10_df = rt_df[rt_df['Trial']==10]

    # #Compute Some statistical indicators of dataframe
    # # Compute A) Mean
    # mean_df = rt_df.groupby('Trial').mean().reset_index()
    # print("Mean =\n", mean_df)
    # # Compute B) Standard Deviation
    # std_df = rt_df.groupby('Trial').std().reset_index()
    # print("STD =\n", std_df)
    # # Compute C) Max
    # max_df = rt_df.groupby('Trial').max().reset_index()
    # print("Max =\n", max_df)
    # # Compute D) Min
    # min_df = rt_df.groupby('Trial').min().reset_index()
    # print("Min =\n", min_df)

    # # IDEA N°1: The Swivel Angle decreases with each trial for each participant 
    # # Abscissa : N° trial
    # # Ordinate : A) Mean Swivel Angle B) SD Swivel Angle C) Max Swivel Angle D) Min Swivel Angle

    # fig5 = plt.figure(figsize=(20,20))
    # ax51 = fig5.add_subplot(211)
    # ax51.set_title("Subject n°"+str(subject_name)+" - IDEA N°1 -  Swivel Angle")
    # ax51.set_ylabel("Mean angle (°)")
    # ax51.set_xlabel("Trial n°")
    # ax51.set_xlim(auto=True)
    # ax51.set_ylim(auto=True)
    # ax51.plot(mean_df['Trial'],mean_df['SwivelAngle'],label='Mean Swivel Angle', color='blue')
    # ax51.plot(max_df['Trial'],max_df['SwivelAngle'],label='Max Swivel Angle', color='red')
    # ax51.plot(min_df['Trial'],min_df['SwivelAngle'],label='Min Swivel Angle', color='green')
    # ax51.legend()
    # ax52 = fig5.add_subplot(212)
    # # ax52.set_title("Subject n°"+str(subject_name))
    # ax52.set_ylabel("Angle SD (°)")
    # ax52.set_xlabel("Trial n°")
    # ax52.set_xlim(auto=True)
    # ax52.set_ylim(auto=True)
    # ax52.plot(std_df['Trial'],std_df['SwivelAngle'],label='Standard Deviation Swivel Angle', color='green')
    # ax52.legend()
    # fig5.savefig(export_idea_graphs+"Idea_1.png")
    # # plt.show()

    # # IDEA N°2: The Twist Angle is anarchic for the first trials and then stabilizes on a value for each participant.
    # # Abscissa : N° trial
    # # Ordinate : A) Mean Twist Angle B) SD Twist Angle

    # fig6 = plt.figure(figsize=(20,20))
    # ax61 = fig6.add_subplot(211)
    # ax61.set_title("Subject n°"+str(subject_name)+" - IDEA N°2 -  Twist Angle")
    # ax61.set_ylabel("Mean angle (°)")
    # ax61.set_xlabel("Trial n°")
    # ax61.set_xlim(auto=True)
    # ax61.set_ylim(auto=True)
    # ax61.plot(mean_df['Trial'],mean_df['TwistAngle'],label='Mean Twist Angle', color='blue')
    # ax61.plot(max_df['Trial'],max_df['TwistAngle'],label='Max Twist Angle', color='red')
    # ax61.plot(min_df['Trial'],min_df['TwistAngle'],label='Min Twist Angle', color='green')
    # ax61.legend()
    # ax62 = fig6.add_subplot(212)
    # # ax52.set_title("Subject n°"+str(subject_name))
    # ax62.set_ylabel("Angle SD (°)")
    # ax62.set_xlabel("Trial n°")
    # ax62.set_xlim(auto=True)
    # ax62.set_ylim(auto=True)
    # ax62.plot(std_df['Trial'],std_df['TwistAngle'],label='Standard Deviation Twist Angle', color='green')
    # ax62.legend()
    # fig6.savefig(export_idea_graphs+"Idea_2.png")
    # # plt.show()

    # # IDEA N°3: The increase in execution speed is directly linked to an increase in Swivel Angle 
    # # Abscissa : Velocity
    # # Ordinate : Swivel Angle
    # fig7 = plt.figure(figsize=(20,20))
    # ax71 = fig7.add_subplot(111)
    # ax71.set_title("Subject n°"+str(subject_name)+" - IDEA N°3 -  Swivel Angle v. Velocity ")
    # ax71.set_ylabel("Swivel angle (°)")
    # ax71.set_xlabel("Velocity (m/s)")
    # ax71.set_xlim(auto=True)
    # ax71.set_ylim(auto=True)
    # ax71 = sns.lineplot(x="Vel", y="SwivelAngle", hue="Trial", data=rt_df, legend='full', palette = palette)
    # ax71.legend()
    # fig7.savefig(export_idea_graphs+"Idea_3.png")

    # # Plot only trial n°10
    # fig7b = plt.figure(figsize=(20,20))
    # ax71b = fig7b.add_subplot(111)
    # ax71b.set_title("Subject n°"+str(subject_name)+" - Trial N°10 - IDEA N°3 -  Swivel Angle v. Velocity ")
    # ax71b.set_ylabel("Swivel angle (°)")
    # ax71b.set_xlabel("Velocity (m/s)")
    # ax71b.set_xlim(auto=True)
    # ax71b.set_ylim(auto=True)
    # ax71b.plot(rt_10_df['Vel'],rt_10_df['SwivelAngle'],label='Swivel Angle', color='green')
    # ax71b.legend()
    # fig7b.savefig(export_idea_graphs+"Trial_10-Idea_3.png")
    # # plt.show()

    # # IDEA N°4: The increase in the feed rate error is directly related to an increase in the Swivel Angle 
    # # Abscissa : Velocity
    # # Ordinate : Swivel Angle
    # rt_df['s_err'] = abs(rt_df['s']-rt_df['s_c'])
    # fig8 = plt.figure(figsize=(20,20))
    # ax81 = fig8.add_subplot(111)
    # ax81.set_title("Subject n°"+str(subject_name)+" - IDEA N°4 -  Feed Rate Error v. Swivel Angle ")
    # ax81.set_ylabel("Swivel angle (°)")
    # ax81.set_xlabel("Feed Rate Error ")
    # ax81.set_xlim(auto=True)
    # ax81.set_ylim(auto=True)
    # # ax81.plot(rt_df['s_err'],rt_df['SwivelAngle'],label='Swivel Angle', color='green')
    # ax81 = sns.lineplot(x="s_err", y="SwivelAngle", hue="Trial", data=rt_df, legend='full', palette = palette) # Separate visually the trials
    # ax81.legend()
    # fig8.savefig(export_idea_graphs+"Idea_4.png")

    # # Plot only trial n°10
    # rt_10_df['s_err'] = abs(rt_10_df['s']-rt_10_df['s_c'])
    # fig8b = plt.figure(figsize=(20,20))
    # ax81b = fig8b.add_subplot(111)
    # ax81b.set_title("Subject n°"+str(subject_name)+" - Trial N°10 - IDEA N°4 -  Feed Rate Error v. Swivel Angle ")
    # ax81b.set_ylabel("Swivel angle (°)")
    # ax81b.set_xlabel("Feed Rate Error ")
    # ax81b.set_xlim(auto=True)
    # ax81b.set_ylim(auto=True)
    # ax81b.plot(rt_10_df['s_err'],rt_10_df['SwivelAngle'],label='Swivel Angle', color='green')
    # ax81b.legend()
    # fig8b.savefig(export_idea_graphs+"Trial_10-Idea_4.png")
    # # plt.show()

    # # Plot Swivel Angle
    # fig9 = plt.figure(figsize=(20,20))
    # ax91 = fig9.add_subplot(111)
    # ax91.set_title("Subject n°"+str(subject_name)+" - Swivel Angle ")
    # ax91.set_ylabel("Swivel angle (°)")
    # ax91.set_xlabel("Curvilinear Abscissa S ")
    # ax91.set_xlim(auto=True)
    # ax91.set_ylim(auto=True)
    # # ax91.plot(rt_df['s'],rt_df['SwivelAngle'],label='Swivel Angle', color='green')
    # ax91 = sns.lineplot(x="s", y="SwivelAngle", hue="Trial", data=rt_df, legend='full', palette = palette) # Separate visually the trials
    # ax91.legend()
    # fig9.savefig(export_idea_graphs+"SwivelAngle.png")
    

    # #####################################################
    # # Plot Data
    # #####################################################
    # print('Ready to plot !')
    # plt.rc('xtick', labelsize=20) 
    # plt.rc('ytick', labelsize=20) 
    # palette = sns.color_palette("Spectral", as_cmap=True)
    # palette28 = sns.color_palette("Spectral", n_colors = 28,  as_cmap=False)

    # rt_df = pd.read_csv(export_folder + 'RT_'+str(trial_start)+'-'+str(trial_end)+'.csv',  delimiter = ',')
    # rt_df['Trial']=rt_df['Trial'].astype(int)
    # print(rt_df)
    # print('Dataframe loaded')

    # fig1 = plt.figure()
    # ax1 = fig1.add_subplot(2, 1, 1)
    # ax1 = sns.lineplot(x="s", y="SwivelAngle", hue="Trial", data=rt_df, legend='full', palette = palette)
    # ax1.set_title(f'Swivel angle for all the trials ',fontsize=22)
    # ax1.set_xlabel('Curvilinear Abscissa s',fontsize=22)
    # ax1.set_ylabel('Swivel Angle (°)',fontsize=22)

    # ax2 = fig1.add_subplot(2, 1, 2)
    # ax2 = sns.lineplot(x="s", y="TwistAngle", hue="Trial", data=rt_df, legend='full', palette = palette)
    # ax2.set_title(f'Twist angle for all the trials ',fontsize=22)
    # ax2.set_xlabel('Curvilinear Abscissa s',fontsize=22)
    # ax2.set_ylabel('Twist Angle (°)',fontsize=22)

    # plt.subplots_adjust(hspace = 0.7)

    # # In[]
    # ########################################"
    # # Statistics 

    # swivel_angle_max = rt_df['SwivelAngle'].max()
    # swivel_angle_min = rt_df['SwivelAngle'].min()
    # twist_angle_max = rt_df['TwistAngle'].max()
    # twist_angle_min = rt_df['TwistAngle'].min()

    # print('swivel_angle_max = ', swivel_angle_max, '°')
    # print('swivel_angle_min = ', swivel_angle_min, '°')
    # print('swivel_angle_MEDIAN = ', (swivel_angle_max+swivel_angle_min)/2, '°')
    # print('twist_angle_max = ', twist_angle_max, '°')
    # print('twist_angle_min = ', twist_angle_min, '°')
    # print('twist_angle_MEDIAN = ', (twist_angle_max+twist_angle_min)/2, '°')


    # stat_df = pd.DataFrame()

    # s_list = np.arange(0.05,0.95, 0.001)

    # for s in s_list:
    #     s = s.round(3)
    #     # print("s = ",s)
    #     try:
    #         s_df = rt_df[rt_df['s'].round(3) == s]
    #         # print('s_df=',s_df)
    #         std_swivel = s_df['SwivelAngle'].std()
    #         std_twist = s_df['TwistAngle'].std()
    #         # print('std_val=',std_val)
    #         mean_swivel = s_df['SwivelAngle'].mean()
    #         mean_twist = s_df['TwistAngle'].mean()
    #         # print('mean_val=',mean_val)
    #         stat_df = stat_df.append(pd.DataFrame({'s':[s],'std_swivel':[std_swivel],'mean_swivel':[mean_swivel],'std_twist':[std_twist],'mean_twist':[mean_twist]}))

    #     except:
    #         print("Error for s=", s)
    # print(stat_df)

    # fig2 = plt.figure()
    # ax1 = fig2.add_subplot(2, 1, 1)
    # # ax1 = sns.lineplot(x="s", y="std_swivel", data=stat_df, legend='full', palette = palette)
    # # ax1 = sns.lineplot(x="s", y="std_twist", data=stat_df, legend='full', palette = palette)
    # ax1.plot(stat_df['s'],stat_df['std_swivel'],label='Swivel Angle')
    # ax1.plot(stat_df['s'],stat_df['std_twist'],label='Twist Angle')
    # ax1.set_title(f'Standard Deviation, Trials '+str(trial_start)+'-'+str(trial_end),fontsize=22)
    # ax1.set_xlabel('Curvilinear Abscissa s',fontsize=22)
    # ax1.set_ylabel('STD (°)',fontsize=22)
    # ax1.legend()

    # # fig2 = plt.figure()
    # ax2 = fig2.add_subplot(2, 1, 2)
    # # ax2 = sns.lineplot(x="s", y="mean_swivel", data=stat_df, legend='full', palette = palette)
    # # ax2 = sns.lineplot(x="s", y="mean_twist", data=stat_df, legend='full', palette = palette)
    # ax2.plot(stat_df['s'],stat_df['mean_swivel'],label='Swivel Angle')
    # ax2.plot(stat_df['s'],stat_df['mean_twist'],label='Twist Angle')
    # ax2.set_title(f'Mean, Trials '+str(trial_start)+'-'+str(trial_end),fontsize=22)
    # ax2.set_xlabel('Curvilinear Abscissa s',fontsize=22)
    # ax2.set_ylabel('Mean (°)',fontsize=22)
    # ax2.legend()
    # plt.legend(fontsize='15', title_fontsize='40')


    # # In[]
    # ##############################################
    # # Inner Statistical
    # # For each trial, compute Mean, RMS, STD, Min, Max, Median
    # fig3 = plt.figure()
    # sns.set(font_scale = 2)

    # plt.rcParams['xtick.labelsize'] = 10 
    # plt.rcParams['ytick.labelsize'] = 20 

    # # rt_df['SwivelAngle']

    # # Mean, Min, Max, Median Swivel Angle
    # ax2 = fig3.add_subplot(2, 2, 1)
    # ax2 = sns.boxplot(x="Trial", y="SwivelAngle",data=rt_df, showmeans=True, meanprops={"marker": "+",
    #                        "markeredgecolor": "red",
    #                        "markersize": "20"}).set(title='Box Plot Swivel Angle',xlabel='Trial', ylabel='Swivel Angle (°)')
    # # ax2 = sns.catplot(x="Trial", y="SwivelAngle",data=rt_df, kind='boxen').set(title='Box Plot Swivel Angle')
    # # ax2 = sns.despine(offset=10, trim=True)
    # plt.yticks(range(round(rt_df['SwivelAngle'].min()),round(rt_df['SwivelAngle'].max()),5))
    # plt.grid(visible=True, which= 'both')

    # # Mean, Min, Max, Median Twist Angle
    # ax3 = fig3.add_subplot(2, 2, 3)
    # ax3 = sns.boxplot(x="Trial", y="TwistAngle",data=rt_df, showmeans=True, meanprops={"marker": "+",
    #                        "markeredgecolor": "red",
    #                        "markersize": "20"}).set(title='Box Plot Twist Angle',xlabel='Trial', ylabel='Twist Angle (°)')
    # # ax3 = sns.catplot(x="Trial", y="TwistAngle",data=rt_df, kind='boxen').set(title='Box Plot Twist Angle')
    # # ax3 = sns.despine(offset=10, trim=True)
    # plt.yticks(range(round(rt_df['TwistAngle'].min()),round(rt_df['TwistAngle'].max()),5))
    # plt.grid(visible=True, which= 'both')

    # # Standard Deviation Computation
    # # sns.relplot(x="Trial", y="SwivelAngle", data=rt_df, kind="line",ci="sd")
    # std_df = rt_df.groupby('Trial').std()
    # print('std_df=',std_df)
    # print('std_df=',std_df.columns)

    # # STD Swivel Angle
    # ax2 = fig3.add_subplot(2, 2, 2)
    # ax2.plot(std_df['SwivelAngle'], linestyle='', marker ='o')
    # ax2.set_title(f'Standard Deviation',fontsize=22)
    # ax2.set_xlabel('Trial',fontsize=22)
    # ax2.set_ylabel('STD (°)',fontsize=22)
    # plt.xticks(range(4,28,1))
    # plt.grid(visible=True, which= 'both')

    # # STD Swivel Angle
    # ax4 = fig3.add_subplot(2, 2, 4)
    # ax4.plot(std_df['TwistAngle'], linestyle='', marker ='o')
    # ax4.set_title(f'Standard Deviation',fontsize=22)
    # ax4.set_xlabel('Trial',fontsize=22)
    # ax4.set_ylabel('STD (°)',fontsize=22)
    # plt.xticks(range(4,28,1))
    # plt.grid(visible=True, which= 'both')


    # plt.subplots_adjust(hspace = 0.5)

    # # XY Plot Essai 4
    # trial_plot = 4
    # fig4 = plt.figure()
    # ax41 = fig4.add_subplot(1,1,1)
    # ax41 = sns.relplot(x="SwivelAngle", y="TwistAngle", hue='s',data=rt_df[rt_df['Trial']==trial_plot], palette = palette, edgecolor = None).set(title= 'Swivel v Twist Angles values for trial n°'+str(trial_plot),xlabel='Swivel Angle (°)', ylabel ='Twist Angle (°)')
    # plt.grid(visible=True, which= 'both')

    # # XY Plot

    # fig5 = plt.figure()
    # ax51 = fig5.add_subplot(1,1,1)
    # ax51 = sns.relplot(x="SwivelAngle", y="TwistAngle", hue='Trial',data=rt_df, palette = palette, legend='full', edgecolor = None).set(title= 'Swivel v Twist Angles values',xlabel='Swivel Angle (°)', ylabel ='Twist Angle (°)')
    # plt.grid(visible=True, which= 'both')

    # # In[]
    # plt.show()

