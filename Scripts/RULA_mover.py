
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from scipy import linalg
from spatialmath import *
from spatialmath.base import trnorm

import RULA

#####################################################
def Distance(A,B):
    d = np.sqrt((A[0]-B[0])**2 + (A[1]-B[1])**2 + (A[2]-B[2])**2)
    return d

############################################################################################
#  define the function which return and export twist dataframe
def RULADataFrame(mot_file_path):
    ###############################################
    # Import Data
    print(mot_file_path)

    # Extract parameter header (transpose it and use names as header)
    print('- Extract Parameters:\n')
    parameters = pd.read_csv(mot_file_path, nrows=4, header = None, skiprows=1, delimiter='=').transpose()
    parameters.columns = parameters.iloc[0]
    parameters = parameters[1:]
    print(parameters)


    # Extract parameter header (transpose it and use names as header)
    print('- Extract data:\n')
    data = pd.read_csv(mot_file_path, header = 0, skiprows=10, delimiter='\t')
    print(data)

    # Rename columns to fit with RULA.py file
    data.rename(columns={'pelvis_tilt':'pelvis_tilt',
                         'pelvis_list':'pelvis_list',
                         'pelvis_rotation':'pelvis_rotation',
                         'pelvis_tx':'pelvis_tx',
                         'pelvis_ty':'pelvis_ty',
                         'pelvis_tz':'pelvis_tz',
                         'lumbar_extension':'lumbar_extension',
                         'lumbar_bending':'UpTrunk_Flx',
                         'lumbar_rotation':'lumbar_rotation',
                         'arm_flex_r':'RightArm_Flx',
                         'arm_add_r':'RightArm_Abd',
                         'arm_rot_r':'RightArm_Rot',
                         'elbow_flex_r':'RightForeArm_Flx',
                         'pro_sup_r':'RightForeArm_Prn',
                         'wrist_flex_r':'RightHand_Flx',
                         'wrist_dev_r':'RightHand_Abd',
                         'arm_flex_l':'LeftArm_Flx',
                         'arm_add_l':'LeftArm_Abd',
                         'arm_rot_l':'LeftArm_Rot',
                         'elbow_flex_l':'LeftForeArm_Flx',
                         'pro_sup_l':'LeftForeArm_Prn',
                         'wrist_flex_l':'LeftHand_Flx',
                         'wrist_dev_l':'LeftHand_Abd'}, inplace=True) 
    # Reverse some columns to respect flexion<->extension and abduction<->adduction
    data['RightArm_Abd'] = -data['RightArm_Abd']
    data['LeftArm_Abd'] = -data['LeftArm_Abd']

    # Create columns without data
    data['RightClav_Elv'] = 0 * data['time']
    data['LeftClav_Elv'] = 0 * data['time']

    print(data)

    # Start RULA processor
    rp = RULA.RULAprocessor()
    rp.setTask(False,False,True,False) # Repetitivity is set here at True, because the measured task is designed to be repetitive
    rp.setLoad(0.0)

    # Compute RULA Score A
    stpNb = data.shape[0] # nb de steps (values)
    data_rula = pd.DataFrame(columns=['Time','RULA'])
    data_rula['Time']=data['time']

    for i in range(stpNb):
        pstA = data[:].iloc[i]
        rp.setPosture(pstA)
        A_scores = rp.getAScores()
        rula_score = rp.getFinalScoreUpperBodyOnly()
        data_rula["RULA"][i]=rula_score


    return data_rula
    


