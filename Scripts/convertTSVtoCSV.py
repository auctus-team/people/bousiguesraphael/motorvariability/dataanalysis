#!/usr/bin/python3.5
import os
import re
import time


# convertQTMtimeCSV("Participant_1_Setup_0_Seq_0_Trial_1.csv")

def convertTSVtoCSV(file, dir=''):
    name = file  # copy the file name
    if dir != '':
        os.chdir(dir)
    print("We convert : " + name + " from .tsv into .csv")
    name = re.sub(r"\b({})\b".format(".tsv"), ".csv", name)
    print("Opening " + file)
    file = open(file, "r")
    print("reading...")
    content = file.read()
    print("Converting...")
    content = re.sub(',', '.', content)
    content = re.sub('\t', ',', content)
    print("Creating " + name)
    new_file = open(name, "w")
    new_file.write(content)
    print("Work done, closing files...")
    file.close()
    new_file.close()
    print("End")

def convertCSVtoTSV(file, dir=''):
    name = file  # copy the file name
    if dir != '':
        os.chdir(dir)
    print("We convert : " + name + " from .csv into .tsv")
    name = re.sub(r"\b({})\b".format(".csv"), ".tsv", name)
    print("Opening " + file)
    file = open(file, "r")
    print("reading...")
    content = file.read()
    print("Converting...")
    content = re.sub(',', '\t', content)
    print("Creating " + name)
    new_file = open(name, "w")
    new_file.write(content)
    print("Work done, closing files...")
    file.close()
    new_file.close()
    print("End")


def convertQTMtimeCSV(file):
    os.chdir("/home/nicolas/Documents/Work/18-01-2022/expeTestRaph/Qualisys/Test")
    chaine = "TIME_STAMP"
    print("Opening the file...")
    file = open(file, "r")
    print("Creating a new file for the copy...")
    new_file = open("new_file.csv", "w")
    print("Going through the file...")
    for line in file:
        if chaine in line:
            print("Timestamp found")
            my_line = line.split(",")
            print(my_line)
            # récupère le uptime du fichier
            old_time = float(str(my_line[3])[:-1])
            print(old_time)
            my_time = time.time()  # test de timestamp
            my_line[3] = str(my_time) + "\n"
            print(my_line)
            line = ','.join([str(elem) for elem in my_line])
            print("Timestamp changed")
            print(line)
        new_file.write(line)
    print("Closing files...")
    file.close
    new_file.close
    print("End")


convertCSVtoTSV('/home/raphael/Documents/MOVER/Data/Mover202207/Qualisys/1543/0001-copy_holed.csv')