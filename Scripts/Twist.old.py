
import numpy as np
import pandas as pd

from scipy import linalg
from spatialmath import *
from spatialmath.base import trnorm

#####################################################
def Distance(A,B):
    d = np.sqrt((A[0]-B[0])**2 + (A[1]-B[1])**2 + (A[2]-B[2])**2)
    return d


# define the Twist function which return twist angle
def TwistAngle(HANS_R,HANF_R,HANB_R,)
############################################################################################
# Compute the transformation matrix corresponding to the frame in the center of the handle
# Coordinates of HANS, HANF and HANB in the handle (real) frame {r}

HANS_R = np.array([1e-10,0.015,0.0525])
HANF_R = np.array([0.02815,1e-10,0.01625])
HANB_R = np.array([-0.02815,1e-10,-0.01625])



# Coordinates of HANS, HANF and HANB in the world frame {0}

# Frame n°1, trial n°1, Subject n°1543
HANB_0 = np.array([0.499518,-0.191197,0.189644])
HANF_0 = np.array([0.491111,-0.125522,0.189784])
HANS_0 = np.array([0.523029,-0.128387,0.229407])
# Test also with
# # Frame n°2302, trial n°30, Subject n°6726
# HANB_0 = np.array([0.237474,-0.205251,0.302858])
# HANF_0 = np.array([0.235752,-0.138958,0.304181])
# HANS_0 = np.array([0.230156,-0.148036,0.340882])
# Frame n°2830, trial n°32, Subject n°3549
# HANB_0 = np.array([0.248434,-0.198139,0.324994])
# HANF_0 = np.array([0.243416,-0.138943,0.300067])
# HANS_0 = np.array([0.221865,-0.131291,0.351259])
# Frame n°1138, trial n°19, Subject n°7961
# HANB_0 = np.array([0.466965,-0.184028,0.239437])
# HANF_0 = np.array([0.446664,-0.12525,0.232621])
# HANS_0 = np.array([0.478654,-0.117846,0.232621])

# Rename the coordinates
[rxs, rys, rzs] = HANS_R
[rxf, ryf, rzf] = HANF_R
[rxb, ryb, rzb] = HANB_R
[oxs, oys, ozs] = HANS_0
[oxf, oyf, ozf] = HANF_0
[oxb, oyb, ozb] = HANB_0

# Base frame {0}

oxo = np.array([1,0,0])
oyo = np.array([0,1,0])
ozo = np.array([0,0,1])


print("d0 BF=\n",Distance(HANF_0,HANB_0),"\tdR BF=\n",Distance(HANF_R,HANB_R) )
print("d0 BS=\n",Distance(HANS_0,HANB_0),"\ndR BS=\n",Distance(HANS_R,HANB_R))
print("d0 SF=\n",Distance(HANS_0,HANB_0),"\tdR SF=\n",Distance(HANS_R,HANB_R))

############################################################################################
# Compute transformation matrix:
############################################################################################
# Translation vector corresponds to the point [0,0,0] in Control frame, and ot_ = [otx, oty, otz] in world frame, the mean position of HANB and HANF in space 

otr = np.array([(HANF_0 + HANB_0)/2])
print("otr=\n",otr)

# Use linalg to compute rotation matrix M thanks to the vector HANS, HANF, HANB in world frame and in real frame :

# System matrix
M = np.array([[rxs, rys, rzs],[rxf, ryf, rzf],[rxb, ryb, rzb]])
# Solve X system
ox_ = np.array([oxs, oxf, oxb])
r1_ =np.linalg.solve(M,ox_-otr[0][0]) # ox_ - tx
# Solve Y system
oy_ = np.array([oys, oyf, oyb])
r2_ =np.linalg.solve(M,oy_-otr[0][1]) # oy_ - ty
# Solve Z system
oz_ = np.array([ozs, ozf, ozb])
r3_ =np.linalg.solve(M,oz_-otr[0][2]) # oz_ - tz

oRr_raw = np.array([r1_,r2_,r3_]) # raw rotation matrix, not orthonal

# Build unit vector of frame {r} in {0}
oxr = np.array([oRr_raw[0][0], oRr_raw[1][0],oRr_raw[2][0]]) # vector u expressed in {0}
oyr = np.array([oRr_raw[0][1], oRr_raw[1][1],oRr_raw[2][1]]) # vector v expressed in {0}
ozr = np.array([oRr_raw[0][2], oRr_raw[1][2],oRr_raw[2][2]]) # vector w expressed in {0}

# Make unit vector orthogonal
# Build v, then u, w is the reference (best performances on 4 )
oyr = np.cross(ozr, oxr) # rebuild v, orthogonal to u and w
oxr = np.cross(oyr,ozr) # rebuild u, orthogonal to v and w
# Build u, then w, v is the reference
# oxr = np.cross(oyr,ozr) # rebuild u, orthogonal to v and w
# ozr = np.cross(oxr, oyr) # rebuild w, orthogonal to u and v
# Build w, then u, v is the reference
# ozr = np.cross(oxr, oyr) # rebuild w, orthogonal to u and v
# oxr = np.cross(oyr,ozr) # rebuild u, orthogonal to v and w
# Build v, then w, u is the reference
# oyr = np.cross(ozr, oxr) # rebuild v, orthogonal to w and u
# ozr = np.cross(oxr,oyr) # rebuild w, orthogonal to u and v
# Build u, then v, w is the reference
# oxr = np.cross(oyr,ozr) # rebuild u, orthogonal to v and w
# oyr = np.cross(ozr, oxr) # rebuild v, orthogonal to w and u
# Build w, then v, u is the reference
# ozr = np.cross(oxr,oyr) # rebuild w, orthogonal to u and v
# oyr = np.cross(ozr, oxr) # rebuild v, orthogonal to w and u

# normalize the unit vectors
oxr = oxr / linalg.norm(oxr) 
oyr = oyr / linalg.norm(oyr)
ozr = ozr / linalg.norm(ozr)

# Build rotation matrix
oRr = np.column_stack((oxr,oyr,ozr))
print("\noRr=\n",oRr)
print("\noRr det =\n",linalg.det(oRr))

# The transformation matrix is:
oTr = np.vstack((np.hstack((oRr, otr.transpose())),[[0,0,0,1]]))
print("oTr = \n",oTr)

# Test 
VB_R = np.append(HANB_R,1)
VF_R = np.append(HANF_R,1)
VS_R = np.append(HANS_R,1)
print("HANB_0 =\n", np.dot(oTr, VB_R))
print("HANF_0 =\n", np.dot(oTr, VF_R))
print("HANS_0 =\n", np.dot(oTr, VS_R))

############################################################################################
# Generate the control frame {c}: it share the y axis with {r}, the z axis of {c} is on a vertical plane, pointing up
############################################################################################
# Create intermediate frame {p} from the projection yr on plan (O,x0,y0):
oyp = np.dot(oyr,oxo) * oxo + np.dot(oyr,oyo) * oyo
oyp = oyp / linalg.norm(oyp) # normalize
ozp = ozo # already normalized
oxp = np.cross(oyp, ozp) 
oxp = oxp / linalg.norm(oxp) # normalize

# print("oxp=",oxp)
# print("oyp=",oyp)
# print("ozp=",ozp)

############################################################################################
# Two first hypothesis to create {c} frame: oyc = oyr and oxc = oxp; then ozc is created from the previous vectors oxc and oyc; 
# t vector is the same than for real frame {r}: otr = otc
oxc = oxp
oyc = oyr
ozc = np.cross(oxc, oyc) 
ozc = ozc / linalg.norm(ozc) # normalize

# print("oxc =",oxc)
# print("oyc =",oyc)
# print("ozc =",ozc)
# print("ozc T =",type(ozc.reshape(-1,1)))
colx = oxc.reshape(-1,1)
coly = oyc.reshape(-1,1)
colz = ozc.reshape(-1,1)
colt = otr.transpose()
# Recontitute the SE3 matrix
oTc = np.vstack((np.concatenate((colx,coly,colz, colt), axis=1),[[0,0,0,1]]))
# print("oTc =\n",oTc)


############################################################################################
# Compute rotation from oTr to oTc (so the rotation angle around yr=yc, giving the Twist angle)

# Use of SpatialMathPackage
# transform the np.array oTc in a SE3 matrix
oTc=SE3(trnorm(oTc))
# print("oTc =\n",oTc)
# transform the np.array oTr in a SE3 matrix

oTr=SE3(trnorm(trnorm(oTr))) # run trnorm twice to ensure normalization (which fail with only one execution of trnorm)
# print("oTr =\n",oTr)

# Create the transformation matric from {r} to {c}
cTr = oTc.inv() * oTr
# print("cTr =\n",cTr)

# compute rotation angle and vector from from {r} to {c}
TwistAngleVec = cTr.angvec()
TwistAxe = TwistAngleVec[1]
TwistAngle = TwistAngleVec[0]
# print("TwistAngle =\n",TwistAngle, "rad", "\t/ ",TwistAngle * 180 / np.pi, "°")
# print("TwistAxe =\n",TwistAxe)
# print("TwistAxe norm =\n",linalg.norm(TwistAxe))
# print("Yr =\n",oyr)
# print("Yr norm =\n",linalg.norm(oyr))





