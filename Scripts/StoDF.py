import pandas as pd

def s_to_df(s, dataframe):
    out_df = pd.DataFrame()
    for i in range(dataframe.shape[0]-1):
        s_i_0 = dataframe['s'][i]
        s_i_1 = dataframe['s'][i+1]
        if s == s_i_0:
            new_row = dataframe[dataframe.index == i].reset_index(drop=True) 
            out_df = pd.concat([out_df, new_row], ignore_index=True) 
        elif(s_i_0 < s and s_i_1 > s) or (s_i_0 > s and s_i_1 < s):
            new_row = pd.DataFrame()
            c = (s - s_i_0)/(s_i_1 - s_i_0)
            new_row = dataframe[dataframe.index == i].reset_index(drop=True) + c *(dataframe[dataframe.index == i+1].reset_index(drop=True) - dataframe[dataframe.index == i].reset_index(drop=True))
            out_df = pd.concat([out_df, new_row], ignore_index=True) 
    # print("out_df:\n",out_df)
    return out_df



