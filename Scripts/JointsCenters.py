##################################################################################
# Libraries
from cProfile import label
from doctest import testmod
import opensim as osim
from pyosim.OsimModel import OsimModel 
import scaling_test

import csv
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt

from mpl_toolkits import mplot3d

import sys

##################################################################################

def JointsCenters(subject, trial_str, model_path, input_mocap_file_path, mot_file_path, output_csv_path, output_mocap_file_path):

    # Output file
    output_joints_centers_file = output_mocap_file_path[:-4]+'_joints_centers.csv'

    ##################################################################################################################################
    ##################################################################################################################################
    # COMPUTATION PART #
    ##################################################################################################################################
    ##################################################################################################################################

    ##################################################################################
    # Create model using OSimModel.py
    print("Loading Model")
    model = OsimModel(model_path)
    print("\t-> Model loaded")

    print('\n################################')
    ## Read .mot file
    # Extract Header and trial configuration
    head_array = np.array(pd.read_csv(mot_file_path, nrows = 4, header = 0, delimiter = '\t')).transpose() # keep 4 parameters: version, nrows, ncolumns, indegrees
    # Check if values are given in dregrees or in radians
    print("\nRead paramaters in .mot file")
    parameters = {}
    for row in head_array[0]:
        print('\t',row)
        [parameter, value] = row.split('=')
        parameters[parameter] = value
    print('\n\t-> Parameters loaded')

    # Extract Joints configurations from .mot file
    data = pd.read_csv(mot_file_path, skiprows = 10, delimiter = '\t')

    # If necessary, transform degrees into radians
    ########################
    ## ! WARNING ! # This Script sort columns names containing 'time', '_tx', '_ty' or '_tz' and don't change their unit in dataframe ! ##
    ########################
    if parameters['inDegrees'] == 'yes':
        print('\nWARNING: The values in the .mot file are expressed in DEGREES')
        to_modify = []
        for col in data.columns:
            if not ('time' in col or '_tx' in col or '_ty' in col or '_tz' in col):
                to_modify.append(col)
        data[to_modify] = data[to_modify] * np.pi / 180 # Convert degrees into radians


    #######################################################
    # Compute Shoulder, Elbow and Wrist Centers at each time

    # Prepare new DataFrame
    joints_center_df = pd.DataFrame(columns=['time', 'SHO_C_x','SHO_C_y','SHO_C_z', 'ELB_C_x','ELB_C_y', 'ELB_C_z', 'WRI_C_x', 'WRI_C_y', 'WRI_C_z'])

    # At each time, take 'q' vector (contains also base orientation and position), update model in this configuration, and compute joints centers
    print('\nComputing Shoulder, Elbow and Wrist centers during trial')
    ind = 0 # incremental variable for progress computation
    for row_index in range(data.shape[0]):
        ###
        percentage=(ind/data.shape[0]*100) # Compute progress in computation
        print(f'Progress: {percentage:.2f} %', end='\r') # Print progress
        ind+=1
        ###
        q = np.array(data.iloc[row_index])[1:] # Update q vector for the considered time
        model.setQ(q) # Update model for this joints configuration
        time_row = pd.DataFrame({'time':[data['time'][row_index]]}) # Create time cell
        SHO_row = pd.DataFrame({'SHO_C_x':model.getJointsPositions()['acromial_r'][1][0], 'SHO_C_y':model.getJointsPositions()['acromial_r'][1][1], 'SHO_C_z':model.getJointsPositions()['acromial_r'][1][2]}) # create Shoulder coordinates cells
        ELB_row = pd.DataFrame({'ELB_C_x':model.getJointsPositions()['radioulnar_r'][1][0], 'ELB_C_y':model.getJointsPositions()['radioulnar_r'][1][1], 'ELB_C_z':model.getJointsPositions()['radioulnar_r'][1][2]}) # create ELbow coordinates cells
        WRI_row = pd.DataFrame({'WRI_C_x':model.getJointsPositions()['radius_hand_r'][1][0], 'WRI_C_y':model.getJointsPositions()['radius_hand_r'][1][1], 'WRI_C_z':model.getJointsPositions()['radius_hand_r'][1][2]}) # create Wrist coordinates cells
        # Assembly results in a row
        row_df = pd.concat([time_row,SHO_row,ELB_row,WRI_row], join= 'outer', axis = 1)
        # Add this row to the global dataframe
        joints_center_df = pd.concat([joints_center_df,row_df], ignore_index=True)

    print("\n\t-> Processed\n")

    #######################################################
    # Save joint centers dataframe in a .csv file
    print('\nSaving joint center file:')
    joints_center_df.to_csv(output_joints_centers_file, index=False)
    print('\t-> Data saved in ',output_joints_centers_file)

    #######################################################
    # Append joint centers coordinates as markers in .trc file

    print('\n################################')
    ## Identify the new computed markers to add from the Joints Center Dataframe computed above
    new_markers_list=[]
    for col in joints_center_df.columns:
        if '_x' in col:     # Recognize the marker name by '_x': the X columns of each marker finishes by '_x'
            new_markers_list.append(col[:-2])

    ## Extract Data from the .trc file and split them in multiple dataframes
    print("\nExtracting Data from .trc file: ", input_mocap_file_path)
    # Extract first row of the .trc file
    print('- Extract 1st line')
    header_l1_trc = pd.read_csv(input_mocap_file_path, nrows=1, header = None, delimiter='\t')

    # Extract parameter header
    print('- Extract Parameters')
    parameters = pd.read_csv(input_mocap_file_path, nrows=1, header = 1, skiprows=0, delimiter='\t')

    # if data in .trc file are expressed in [mm], we have to convert joints center also in [mm] (multiply by 1000): the data from OSimModel.py are expressed in [m]
    unit_multiplier = 1 
    if parameters['Units'][0] =='mm': 
        unit_multiplier = 1000
        print('\tWARNING: Data in .trc file are expressed in [mm]')

    # Extract markers names of the .trc file
    print('- Extract markers names')
    markers_trc = pd.read_csv(input_mocap_file_path, nrows=1 , header = None ,skiprows=3)

    # Extract dataframe containing coordinates of the markers
    print('- Extract data')
    data_trc = pd.read_csv(input_mocap_file_path, header = 1, delimiter='\t',skiprows=3)

    ## Operations on these dataframes to add our new data
    # Update NumMarkers
    parameters.loc[0,'NumMarkers'] = parameters.loc[0,'NumMarkers'] + len(new_markers_list)
    print('- New .trc file will contain ',parameters.loc[0,'NumMarkers'], ' markers' )


    print('\nRebuilding a .trc file')
    # Add New Markers/Joints Centers names to the markers names dataframe
    for marker in new_markers_list:
        markers_trc.iloc[0] = markers_trc.iloc[0] +"\t\t\t"+marker # There are 3 tabs before marker names


    # Add New Markers/Joints Centers coordinates at the end of marker dataframe
    for marker in new_markers_list:
        marker_no = int(data_trc.columns[-1][1:])+1 # Compute New columns number
        marker_columns = ['X'+str(marker_no), 'Y'+str(marker_no),'Z'+str(marker_no) ] # Write New columns names
        marker_df = pd.DataFrame({marker_columns[0]:joints_center_df[marker+'_x']*unit_multiplier,  marker_columns[1]:joints_center_df[marker+'_y']*unit_multiplier, marker_columns[2]:joints_center_df[marker+'_z']*unit_multiplier}) # Get in joints_center dataframe the value to put in the .trc dataframe. Don't forget to use 'unit_multiplier' to set the right unit in the .trc file
        data_trc = pd.concat([data_trc,marker_df], join= 'outer', axis = 1) # append the dataframe from the new marker to the global dataframe (here, it appends columns: axis=1)

    # Replace Dirty columns names by null string ''
    data_trc.rename(columns={'Unnamed: 0':'','Unnamed: 1':''}, inplace=True) 

    ## Reconsitute the .trc file
    # Concatenate in the right order the 4 dataframes to obtain the .trc file like the original one
    print('\nSaving the new .trc file...')
    header_l1_trc.to_csv(output_mocap_file_path, index=False, sep='\t', header=False)
    parameters.to_csv(output_mocap_file_path, mode="a", index=False, sep='\t', header=True) # mode "a" means to append dataframe in the .csv file
    markers_trc.to_csv(output_mocap_file_path, mode="a", index=False, sep=' ', header=False) # a simple ' ' [space] string works perfectly to transform the 1 cell dataframe into a multi-cells csv file (the \t are kept in the one cell df)
    data_trc.to_csv(output_mocap_file_path, mode="a", index=False, sep='\t', header=True)
    print('\t-> Data saved in ',output_mocap_file_path)

    ##############################################################################
    # Simplify the saved file in a csv file, without header, and with only columns names renamed like: 
    # \t \t CLAV
    # X1 Y1 Z1      ==> CLAV_x CLAV_y CLAV_z
    # to make post processing and analyse easier
    print('\n################################')
    print('\nBuilding simpler .csv file without header, from saved .trc file')
    # Extract parameter header

    print('- Extract data from .trc file')
    parameters = pd.read_csv(output_mocap_file_path, nrows=1, header = 1, skiprows=0, delimiter='\t')
    n_markers = parameters['NumMarkers'][0]

    # Extract markers names of the .trc file
    markers_trc = pd.read_csv(output_mocap_file_path, nrows=2 , header = None , delimiter='\t',skiprows=3)

    # Extract dataframe containing coordinates of the markers
    data_trc = pd.read_csv(output_mocap_file_path, header = 1, delimiter='\t',skiprows=3)


    # Reconstitute the dataframe with only necessary data:
    print('- Rebuild a dataframe')
    markers_df = pd.DataFrame({markers_trc.iloc[0,0]: data_trc.iloc[:,0], markers_trc.iloc[0,1]: data_trc.iloc[:,1]}) # Keep '#Frame' and 'time' columns
    for marker_no in range(1,n_markers+1):
        marker_name = markers_trc.iloc[0, 2+marker_no*3-1] # Pass the 2 1st columns '#Frame' and 'time', then take de 3rd cell to find the name of the marker (marker_no+1), retrieve 1 because tab start to 0

        markers_df[marker_name+'_x'] = data_trc.iloc[:,2+3*marker_no-2-1] # Pass the 2 first columns, move to marker n°[marker_no] by jumping 3 columns by 3, remove 2 columns to obtain X coordinate, 1 for Y coordinate, 0 for Z coordinate, then retrieve 1 because tab start to 0 
        markers_df[marker_name+'_y'] = data_trc.iloc[:,2+3*marker_no-1-1] # Pass the 2 first columns, move to marker n°[marker_no] by jumping 3 columns by 3, remove 2 columns to obtain X coordinate, 1 for Y coordinate, 0 for Z coordinate, then retrieve 1 because tab start to 0
        markers_df[marker_name+'_z'] = data_trc.iloc[:,2+3*marker_no-0-1] # Pass the 2 first columns, move to marker n°[marker_no] by jumping 3 columns by 3, remove 2 columns to obtain X coordinate, 1 for Y coordinate, 0 for Z coordinate, then retrieve 1 because tab start to 0

    # Save the dataframe to .csv file
    print('\nSaving the .csv file...')
    markers_df.to_csv(output_csv_path, index= False)
    print('\t-> Data saved in ',output_csv_path)

    ##################################################################################################################################
    ##################################################################################################################################
    # VERIFICATION PART #
    ##################################################################################################################################
    ##################################################################################################################################

    # # Import data
    # markers_df = pd.read_csv(output_csv_path)

    # # Plot data to verify coherence of joints centers

    # figure3d_1 = plt.figure(figsize=[4,4])
    # ax1 = figure3d_1.add_subplot(111, projection='3d')
    # ax1.set_xlabel('X')
    # ax1.set_ylabel('Y')
    # ax1.set_zlabel('Z')

    # segments_list = [['SHO_C','ELB_C'],['ELB_C','WRI_C'],['RSHO','BACK'],['LSHO','BACK'],['RSHO','CLAV'],['LSHO','CLAV'],['WRI_C','RWRA'],['WRI_C','RWRB'],['RFINA','RWRA'],['RFINB','RWRB'],['TABR','TABL'],['TABL','TABB'],['TABB','TABR'],['HANS','HANF'],['HANF','HANB'],['HANB','HANS']] # Select points to build segments (bones)

    # # Build 2 lists: 1 for reflective markers of Qualisys, 1 for joints centers markers (virtual markers)
    # point_col = []
    # segment_col = []

    # for col in markers_df.columns[2:]:
    #     print(col)
    #     in_segment_list = False
    #     for segment in segments_list:
    #         if segment[0] in col or segment[1] in col:
    #             in_segment_list = True
    #     if in_segment_list:
    #         segment_col.append(col)
    #     else:
    #         point_col.append(col)

    # print('joints_centers_markers\n',segment_col)
    # print('qualisys_markers\n',point_col)

    # dt = markers_df['Time'][1]-markers_df['Time'][0] # Compute real delay between 2 frames 
    # playback_speed = 10 # accelerate the animation from this factor
    # fps = 15 # frame rate for animation
    # for i in range(0,markers_df.shape[0],int(playback_speed/(fps*dt))):
    # # for i in range(10):
    #     ax1.clear()
    #     # Plot reflective markers during time
    #     for col_no in range(0,len(point_col),3):
    #         if 'TAB' in point_col[col_no]:
    #             color = 'yellow'
    #         elif 'HAN' in point_col[col_no]:
    #             color = 'red'
    #         elif '_C_' in point_col[col_no]:
    #             color = 'green'
    #         else:
    #             color = 'blue'
    #         X_name = point_col[col_no]
    #         Y_name = point_col[col_no+1]
    #         Z_name = point_col[col_no+2]
    #         ax1.scatter(markers_df[X_name][i],markers_df[Y_name][i],markers_df[Z_name][i], color=color)
    #     # plot joints centers during time
    #     for segment in segments_list:
    #         for col_no in range(0,len(segment_col),3):
    #             if segment[0] in segment_col[col_no]:
    #                 X_name = segment_col[col_no]
    #                 Y_name = segment_col[col_no+1]
    #                 Z_name = segment_col[col_no+2]
    #                 X1 = markers_df[X_name][i]
    #                 Y1 = markers_df[Y_name][i]
    #                 Z1 = markers_df[Z_name][i]
    #                 if 'TAB' in segment_col[col_no]:
    #                     color = 'yellow'
    #                 elif 'HAN' in segment_col[col_no]:
    #                     color = 'red'
    #                 elif '_C_' in segment_col[col_no]:
    #                     color = 'green'
    #                 else:
    #                     color = 'blue'
    #             elif segment[1] in segment_col[col_no]:
    #                 X_name = segment_col[col_no]
    #                 Y_name = segment_col[col_no+1]
    #                 Z_name = segment_col[col_no+2]
    #                 X2 = markers_df[X_name][i]
    #                 Y2 = markers_df[Y_name][i]
    #                 Z2 = markers_df[Z_name][i]
    #         ax1.plot([X1,X2], [Y1,Y2], [Z1,Z2], color=color)
    #         ax1.scatter(X1, Y1, Z1, color=color)
    #         ax1.scatter(X2, Y2, Z2, color=color)
        
    #     plt.pause(1/fps)
        

    # # plt.show()

    # ##########################################
    # # Verify Results Quantitatively

    # # Verify the evolution of bones length: humerus and ulna

    # l_humerus = np.sqrt( (markers_df['SHO_C_x']-markers_df['ELB_C_x'])**2 + (markers_df['SHO_C_y']-markers_df['ELB_C_y'])**2 + (markers_df['SHO_C_z']-markers_df['ELB_C_z'])**2 )
    # l_ulna = np.sqrt( (markers_df['WRI_C_x']-markers_df['ELB_C_x'])**2 + (markers_df['WRI_C_y']-markers_df['ELB_C_y'])**2 + (markers_df['WRI_C_z']-markers_df['ELB_C_z'])**2 )

    # fig_verif = plt.figure(figsize=[4,4])

    # # Plot Bones Lengths
    # ax_bl = fig_verif.add_subplot(211)
    # ax_bl.set_title('Subject n°'+subject+" - Trial n°"+trial_str)
    # ax_bl.set_xlabel("Time (s)")
    # ax_bl.set_ylabel("Bone Length (mm)")
    # ax_bl.plot(markers_df['Time'], l_humerus, label='Humerus Length', color='green')
    # ax_bl.plot(markers_df['Time'], l_ulna, label='Ulna Length', color='red')
    # ax_bl.legend()

    # # Verify the constitency of the distance RSHO-SHO_C, RELB-ELB_C, RWRA-WRI_C, RWRB-WRI_C

    # ## Distance RSHO-SHO_C
    # d_SHO = np.sqrt( (markers_df['SHO_C_x']-markers_df['RSHO_x'])**2 + (markers_df['SHO_C_y']-markers_df['RSHO_y'])**2 + (markers_df['SHO_C_z']-markers_df['RSHO_z'])**2 )

    # ## Distance RELB-ELB_C
    # d_ELB = np.sqrt( (markers_df['ELB_C_x']-markers_df['RELB_x'])**2 + (markers_df['ELB_C_y']-markers_df['RELB_y'])**2 + (markers_df['ELB_C_z']-markers_df['RELB_z'])**2 )

    # ## Distance RWRA-WRI_C
    # d_WRI_A = np.sqrt( (markers_df['WRI_C_x']-markers_df['RWRA_x'])**2 + (markers_df['WRI_C_y']-markers_df['RWRA_y'])**2 + (markers_df['WRI_C_z']-markers_df['RWRA_z'])**2 )

    # ## Distance RWRB-WRI_C
    # d_WRI_B = np.sqrt( (markers_df['WRI_C_x']-markers_df['RWRB_x'])**2 + (markers_df['WRI_C_y']-markers_df['RWRB_y'])**2 + (markers_df['WRI_C_z']-markers_df['RWRB_z'])**2 )

    # # Plot Markers Distance
    # ax_md = fig_verif.add_subplot(212)
    # ax_md.set_xlabel("Time (s)")
    # ax_md.set_ylabel("Euclidean Distance (mm)")
    # ax_md.plot(markers_df['Time'], d_SHO, label='Dist. RSHO-SHO_C', color='green')
    # ax_md.plot(markers_df['Time'], d_ELB, label='Dist. RELB-ELB_C', color='blue')
    # ax_md.plot(markers_df['Time'], d_WRI_A, label='Dist. RWRA-WRI_C', color='red')
    # ax_md.plot(markers_df['Time'], d_WRI_B, label='Dist. RWRB-WRI_C', color='black')
    # ax_md.legend()


    # plt.show()