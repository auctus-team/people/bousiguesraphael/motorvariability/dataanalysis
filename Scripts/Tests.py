# In[]
from cProfile import label
from doctest import testmod
import opensim as osim
from pyosim.OsimModel import OsimModel 
import scaling_test

import csv
import numpy as np

import matplotlib.pyplot as plt

from mpl_toolkits import mplot3d




model_path = '/home/raphael/Documents/app/dataanalysis/Models/Rajagopal2015-MOVER-1543.osim'
mocap_filename = '/home/raphael/Documents/app/dataanalysis/Data/0001bis.trc'
scale_setup_filename = '/home/raphael/Documents/app/dataanalysis/Data/OSIM_Scale_1543.xml'

scaled_model_filename = 'Rajagopal2015-MOVER_SCALED.osim'
adjusted_model_filename = 'Rajagopal2015-MOVER_ADJUSTED.osim'
marker_placement_file = 'MarkerPlacement.csv'
markerList =[]


IK_output_filename = 'IK_out.mot'
IK_setup_filename = scale_setup_filename

# model_path = '/home/raphael/opensim-core/Models/MOVER/1543.osim'

my_model = osim.Model(model_path)
# my_model.setUseVisualizer(True)
state = my_model.initSystem()

# Get trc data to determine time range

markerData = osim.MarkerData(mocap_filename)
initial_scale_time = markerData.getStartFrameTime()
final_scale_time = markerData.getLastFrameTime()
timeArray = osim.ArrayDouble()
timeArray.set(0,initial_scale_time)
timeArray.set(1,final_scale_time)

# Test Pyosim
testmodel = OsimModel(model_path)


q = [-18.27751166,48.03992650,-45.65655931,0.48635049,0.15797877,-0.14583582,-30.41183272,29.10122596,-21.22427372,42.20341864,-19.66212984,32.49235728,94.33080689,89.42510749,0.00000000,0.00000000,0.00000000,-6.21616270,0.00000000,59.34640829,47.17770142,0.00000000,0.00000000]

# q = [-30.41183272,29.10122596,-21.22427372,42.20341864,-19.66212984,32.49235728,94.33080689,89.42510749,0.00000000,0.00000000,0.00000000,-6.21616270,0.00000000,59.34640829,47.17770142,0.00000000,0.00000000]
print('q=',q)

testmodel.setQ(q)


markerSet=my_model.getMarkerSet()
for marker in markerSet:
    marker_name = marker.getName()
    # print(marker_name)
    markerList.append(marker_name)
print("Marker List= ", markerList)

# # In[]
# ground_pelvis = my_model.getJointSet().get('ground_pelvis')
# for i in range(ground_pelvis.numCoordinates()):
#     ground_pelvis.get_coordinates(i).setLocked(state, True)


# back = my_model.getJointSet().get('back')
# for i in range(back.numCoordinates()):
#     back.get_coordinates(i).setLocked(state, True)

# ## Create scale tool
# scaleTool = osim.ScaleTool(scale_setup_filename)




# # Tell scale tool to use the loaded model
# scaleTool.getGenericModelMaker().setModelFileName(model_path)

# ## ModelScaler-
# # Whether or not to use the model scaler during scale
# scaleTool.getModelScaler().setApply(True)
# # Set the marker file (.trc) to be used for scaling 
# scaleTool.getModelScaler().setMarkerFileName(mocap_filename)
# # set a time range
# scaleTool.getModelScaler().setTimeRange(timeArray)
# # Indicating whether or not to preserve relative mass between segments
# scaleTool.getModelScaler().setPreserveMassDist(False)
# #Name of OpenSim model file (.osim) to write when done scaling.
# # scaleTool.getModelScaler().setOutputModelFileName(scaled_model_filename)
# # Run model scaler Tool
# scaleTool.getModelScaler().processModel(my_model, scaleTool.getPathToSubject())

# # Update state to current state
# state = my_model.getWorkingState()

# ## ModelPlacer-
# # Whether or not to use the model scaler during scale
# scaleTool.getMarkerPlacer().setApply(True)
# # Set the marker placer time range
# scaleTool.getMarkerPlacer().setTimeRange(timeArray)
# # Set the marker file (.trc) to be used for scaling 
# scaleTool.getMarkerPlacer().setStaticPoseFileName(mocap_filename)
# # scaleTool.getMarkerPlacer().setOutputModelFileName(adjusted_model_filename)
# # Maximum amount of movement allowed in marker data when averaging
# scaleTool.getMarkerPlacer().setMaxMarkerMovement(-1)
# # Run Marker Placer
# scaleTool.getMarkerPlacer().processModel(my_model, '')

# # Update state to current state
# state = my_model.getWorkingState()

# with open(marker_placement_file, 'a') as fout:
#     writer = csv.writer(fout, delimiter = ',')
#     data_marker = []
#     for m in markerList:
#         for j in range(3):
#             data_marker.append(my_model.getMarkerSet().get(m).get_location().get(j))
#     writer.writerow(data_marker)

# Lock torso displacements
# for coord in ['r_x', 'r_y', 'r_z', 't_x', 't_y', 't_z']:
#     c = my_model.getCoordinateSet().get(coord)
#     c.set_default_value(c.getValue(state))
#     c.set_locked(1)

ground_pelvis = my_model.getJointSet().get('ground_pelvis')
for i in range(ground_pelvis.numCoordinates()):
    ground_pelvis.get_coordinates(i).setLocked(state, True)


back = my_model.getJointSet().get('back')
for i in range(back.numCoordinates()):
    back.get_coordinates(i).setLocked(state, True)

## Create IK tool
IK_tool = osim.InverseKinematicsTool(IK_setup_filename)
IK_tool.setModel(my_model)

#Get initial and final time 
IK_tool.setStartTime(markerData.getStartFrameTime())
IK_tool.setEndTime(markerData.getLastFrameTime())

IK_tool.setMarkerDataFileName(mocap_filename)
# IK_tool.setOutputMotionFileName(IK_output_filename)
# IK_tool.run()

print("Okay j'ai tout !")

print("Bodies=",testmodel.getBodiesPositions().keys())
print("Joints=",testmodel.getJointsPositions().keys())
print("Ulna Origin=",testmodel.getBodiesPositions()['ulna_r'][1])
print("Elbow Joint=",testmodel.getJointsPositions()['elbow_r'][1])

fig = plt.figure(figsize=(4,4))
ax = fig.add_subplot(111, projection='3d')
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')
fig2 = plt.figure(figsize=(4,4))
ax2 = fig2.add_subplot(111, projection='3d')
ax2.set_xlabel('X')
ax2.set_ylabel('Y')
ax2.set_zlabel('Z')

# Draw Back
joint1 = 'pelvis'
joint2 = 'torso'
ax.plot([testmodel.getBodiesPositions()[joint1][1][0][0],testmodel.getBodiesPositions()[joint2][1][0][0]],[testmodel.getBodiesPositions()[joint1][1][1][0],testmodel.getBodiesPositions()[joint2][1][1][0]],[testmodel.getBodiesPositions()[joint1][1][2][0],testmodel.getBodiesPositions()[joint2][1][2][0]])
# Draw R Clavicular
joint1 = 'torso'
joint2 = 'humerus_r'
ax.plot([testmodel.getBodiesPositions()[joint1][1][0][0],testmodel.getBodiesPositions()[joint2][1][0][0]],[testmodel.getBodiesPositions()[joint1][1][1][0],testmodel.getBodiesPositions()[joint2][1][1][0]],[testmodel.getBodiesPositions()[joint1][1][2][0],testmodel.getBodiesPositions()[joint2][1][2][0]])
# Draw R Arm
joint1 = 'humerus_r'
joint2 = 'ulna_r'
ax.plot([testmodel.getBodiesPositions()[joint1][1][0][0],testmodel.getBodiesPositions()[joint2][1][0][0]],[testmodel.getBodiesPositions()[joint1][1][1][0],testmodel.getBodiesPositions()[joint2][1][1][0]],[testmodel.getBodiesPositions()[joint1][1][2][0],testmodel.getBodiesPositions()[joint2][1][2][0]])
# Draw R Ulna
joint1 = 'ulna_r'
joint2 = 'hand_r'
ax.plot([testmodel.getBodiesPositions()[joint1][1][0][0],testmodel.getBodiesPositions()[joint2][1][0][0]],[testmodel.getBodiesPositions()[joint1][1][1][0],testmodel.getBodiesPositions()[joint2][1][1][0]],[testmodel.getBodiesPositions()[joint1][1][2][0],testmodel.getBodiesPositions()[joint2][1][2][0]])
# Draw R Humerus
joint1 = 'radius_r'
joint2 = 'hand_r'
ax.plot([testmodel.getBodiesPositions()[joint1][1][0][0],testmodel.getBodiesPositions()[joint2][1][0][0]],[testmodel.getBodiesPositions()[joint1][1][1][0],testmodel.getBodiesPositions()[joint2][1][1][0]],[testmodel.getBodiesPositions()[joint1][1][2][0],testmodel.getBodiesPositions()[joint2][1][2][0]])
# Draw L Clavicular
joint1 = 'torso'
joint2 = 'humerus_l'
ax.plot([testmodel.getBodiesPositions()[joint1][1][0][0],testmodel.getBodiesPositions()[joint2][1][0][0]],[testmodel.getBodiesPositions()[joint1][1][1][0],testmodel.getBodiesPositions()[joint2][1][1][0]],[testmodel.getBodiesPositions()[joint1][1][2][0],testmodel.getBodiesPositions()[joint2][1][2][0]])
# Draw L Arm
joint1 = 'humerus_l'
joint2 = 'ulna_l'
ax.plot([testmodel.getBodiesPositions()[joint1][1][0][0],testmodel.getBodiesPositions()[joint2][1][0][0]],[testmodel.getBodiesPositions()[joint1][1][1][0],testmodel.getBodiesPositions()[joint2][1][1][0]],[testmodel.getBodiesPositions()[joint1][1][2][0],testmodel.getBodiesPositions()[joint2][1][2][0]])
# Draw L Ulna
joint1 = 'ulna_l'
joint2 = 'hand_l'
ax.plot([testmodel.getBodiesPositions()[joint1][1][0][0],testmodel.getBodiesPositions()[joint2][1][0][0]],[testmodel.getBodiesPositions()[joint1][1][1][0],testmodel.getBodiesPositions()[joint2][1][1][0]],[testmodel.getBodiesPositions()[joint1][1][2][0],testmodel.getBodiesPositions()[joint2][1][2][0]])
# Draw L Humerus
joint1 = 'radius_l'
joint2 = 'hand_l'
ax.plot([testmodel.getBodiesPositions()[joint1][1][0][0],testmodel.getBodiesPositions()[joint2][1][0][0]],[testmodel.getBodiesPositions()[joint1][1][1][0],testmodel.getBodiesPositions()[joint2][1][1][0]],[testmodel.getBodiesPositions()[joint1][1][2][0],testmodel.getBodiesPositions()[joint2][1][2][0]])


for joint in testmodel.getJointsPositions().keys():
    ax.scatter(testmodel.getJointsPositions()[joint][1][0],testmodel.getJointsPositions()[joint][1][1],testmodel.getJointsPositions()[joint][1][2], label=joint)

for body in testmodel.getBodiesPositions().keys():
    ax2.scatter(testmodel.getBodiesPositions()[body][1][0],testmodel.getBodiesPositions()[body][1][1],testmodel.getBodiesPositions()[body][1][2], label=body)

ax.legend()
ax2.legend()
plt.show()

###################################

# osim.VisualizerUtilities_showModel(my_model)

# pelvisbody = my_model.getBodySet().get('pelvis')

# print("Model Path : ", model_path)

# my_model.setName("MOVER")
# my_model.setUseVisualizer(True)

# # ---------------------------------------------------------------------------
# # Create two links, each with a mass of 1 kg, center of mass at the body's
# # origin, and moments and products of inertia corresponding to an ellipsoid
# # with radii of 0.1, 0.5 and 0.1, in the x, y and z directions, respectively.
# # ---------------------------------------------------------------------------

# humerus = osim.Body("humerus",
#                     1.0,
#                     osim.Vec3(0),
#                     osim.Inertia(0.052, 0.004, 0.052))
# radius = osim.Body("radius",
#                    1.0,
#                    osim.Vec3(0),
#                    osim.Inertia(0.052, 0.004, 0.052))

# # ---------------------------------------------------------------------------
# # Connect the bodies with pin joints. Assume each body is 1m long.
# # ---------------------------------------------------------------------------

# shoulder = osim.PinJoint("shoulder",
#                          arm.getGround(), # PhysicalFrame
#                          osim.Vec3(0),
#                          osim.Vec3(0),
#                          humerus, # PhysicalFrame
#                          osim.Vec3(0, 0.5, 0),
#                          osim.Vec3(0))

# elbow = osim.PinJoint("elbow",
#                       humerus, # PhysicalFrame
#                       osim.Vec3(0, -0.5, 0),
#                       osim.Vec3(0),
#                       radius, # PhysicalFrame
#                       osim.Vec3(0, 0.5, 0),
#                       osim.Vec3(0))

# # ---------------------------------------------------------------------------
# # Add a muscle that flexes the elbow (actuator for robotics people).
# # ---------------------------------------------------------------------------

# biceps = osim.Millard2012EquilibriumMuscle("biceps",  # Muscle name
#                                            100.0,  # Max isometric force
#                                            0.6,  # Optimal fibre length
#                                            0.55,  # Tendon slack length
#                                            0.0)  # Pennation angle
# biceps.addNewPathPoint("origin",
#                        humerus,
#                        osim.Vec3(0, 0.3, 0))

# biceps.addNewPathPoint("insertion",
#                        radius,
#                        osim.Vec3(0, 0.2, 0))

# # ---------------------------------------------------------------------------
# # Add a controller that specifies the excitation of the muscle.
# # ---------------------------------------------------------------------------

# brain = osim.PrescribedController()
# brain.addActuator(biceps)
# brain.prescribeControlForActuator("biceps",
#                                   osim.StepFunction(0.5, 3.0, 0.3, 1.0))

# # ---------------------------------------------------------------------------
# # Build model with components created above.
# # ---------------------------------------------------------------------------

# arm.addBody(humerus)
# arm.addBody(radius)
# arm.addJoint(shoulder) # Now required in OpenSim4.0
# arm.addJoint(elbow)
# arm.addForce(biceps)
# arm.addController(brain)

# # ---------------------------------------------------------------------------
# # Add a console reporter to print the muscle fibre force and elbow angle.
# # ---------------------------------------------------------------------------

# # We want to write our simulation results to the console.
# reporter = osim.ConsoleReporter()
# reporter.set_report_time_interval(1.0)
# reporter.addToReport(biceps.getOutput("fiber_force"))
# elbow_coord = elbow.getCoordinate().getOutput("value")
# reporter.addToReport(elbow_coord, "elbow_angle")
# arm.addComponent(reporter)

# # ---------------------------------------------------------------------------
# # Add display geometry. 
# # ---------------------------------------------------------------------------

# bodyGeometry = osim.Ellipsoid(0.1, 0.5, 0.1)
# bodyGeometry.setColor(osim.Vec3(0.5)) # Gray
# humerusCenter = osim.PhysicalOffsetFrame()
# humerusCenter.setName("humerusCenter")
# humerusCenter.setParentFrame(humerus)
# humerusCenter.setOffsetTransform(osim.Transform(osim.Vec3(0)))
# humerus.addComponent(humerusCenter)
# humerusCenter.attachGeometry(bodyGeometry.clone())

# radiusCenter = osim.PhysicalOffsetFrame()
# radiusCenter.setName("radiusCenter")
# radiusCenter.setParentFrame(radius)
# radiusCenter.setOffsetTransform(osim.Transform(osim.Vec3(0)))
# radius.addComponent(radiusCenter)
# radiusCenter.attachGeometry(bodyGeometry.clone())

# # ---------------------------------------------------------------------------
# # Configure the model.
# # ---------------------------------------------------------------------------


# print("\nList Bodies:")

# bodyList=my_model.getBodySet()el.getJointsPositions().keys():
#     ax.scatter(testmodel.getJointsPositions()[joint][1][0],testmodel.getJointsPositions()[joint][1][1],testmodel.getJointsPositions()[joint][1][2], label=joint)

# for body in testmodel.getBodiesPositions().keys():
#     ax2.scatter(testmo

# ground_pelvis = my_model.getJointSet().get('ground_pelvis')
# for i in range(ground_pelvis.numCoordinates()):
#     ground_pelvis.get_coordinates(i).setLocked(state, True)


# back = my_model.getJointSet().get('back')
# for i in range(back.numCoordinates()):
#     back.get_coordinates(i).setLocked(state, True)

######################
# Scaling

# print('\nScaling')
# # scale = osim.ScaleTool('OSIM_Scale_1543.xml')
# scaling_test.scale()
# print('\t--> Scaling complete')

# state = my_model.initSystem()
# # # Fix the shoulder at its default angle and begin with the elbow flexed.
# # shoulder.getCoordinate().setLocked(state, True)
# # elbow.getCoordinate().setValue(state, 0.5 * osim.SimTK_PI)
# my_model.equilibrateMuscles(state)

# # ---------------------------------------------------------------------------
# # Configure the visualizer.
# # ---------------------------------------------------------------------------

# viz = my_model.updVisualizer().updSimbodyVisualizer()
# viz.setBackgroundColor(osim.Vec3(0)) # white
# viz.setGroundHeight(-2)

# # # ---------------------------------------------------------------------------
# # # Simulate.
# # # ---------------------------------------------------------------------------

# manager = osim.Manager(my_model)
# state.setTime(0)
# manager.initialize(state)
# state = manager.integrate(5.0)

