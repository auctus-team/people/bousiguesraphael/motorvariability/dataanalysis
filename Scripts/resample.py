import numpy as np
import pandas as pd

# test_df = pd.DataFrame({'Time':[67001.57,67002.63,67003.46,67004.35,67005.66,67006.88,67007.21,67007.93,67008.86,67009.92,67010.94,67011.12],
# 'X':[1.68,1.52,1.67,1.78,2.05,2.13,1.97,2.07,1.67,1.78,1.64,2.11],
# 'Y':[5.01,5.13,4.84,4.81,4.75,4.62,4.33,4.13,4.28,4.05,3.99,3.85],
# 'Z':[10.56,10.54,10.25,10.53,10.36,10.57,10.43,10.36,10.65,10.48,10.44,10.58]})

# test_df['Time'] = test_df['Time']/1000

# print(test_df)
# f_goal = 100

def Resample(df_in, time_col_name, f_out=1, milli=False, new_time_col=[]):
    try:
        if milli: # Time is expressed in milliseconds
            conv_milli=1000
        else:   # Time is expressed in seconds
            conv_milli=1

        new_time_col = np.array(new_time_col) # Convert into np.array, which is easier to work with here

        # 'type_col' could be a string specifying the column of Time, or directly the new time_column
        if new_time_col.size == 0:
            print("Consider frequency f=",f_out,"Hz")
            t_start = np.floor(np.array(df_in[time_col_name])[0] * f_out / conv_milli) *conv_milli / f_out # round to the sample unit
            t_end = np.array(df_in[time_col_name])[-1]
            new_time_col = np.arange(t_start,t_end,conv_milli/f_out)
            new_time_col = np.floor(new_time_col * f_out / conv_milli) *conv_milli / f_out # round to the sample unit

            
        new_df = pd.DataFrame({time_col_name:new_time_col})

        print("Resampling...")
        for col in df_in.columns:
            if col != time_col_name and df_in[col].dtype != object:
                new_df[col] = np.interp(new_time_col,np.array(df_in[time_col_name]),np.array(df_in[col])) #  LINEAR interpolation
        
        
        print("\t-> Data resampled\n")
    except:
        print("Error occurs during resampling\n")
    return new_df

# print("Original Df:\n",test_df)
# resample_df = Resample(test_df,'Time',f_goal, False)
# print("New Df:\n",resample_df)




