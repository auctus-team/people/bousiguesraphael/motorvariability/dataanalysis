import numpy as np
import csv
import os
subject = 'S2'

folder = 'Kinematic_data'
marker_names = {'S1' : ['s1Shoulder', 's1Elbow', 's1Hand1', 's1Hand2', 's1Hand3', 's1Hand4'], 'S2' : ['s2Shoulder', 's2Elbow', 's2Hand1', 's2Hand2', 's2Hand3', 's2Hand4']}

for filename in sorted(os.listdir(folder)):
	with open(folder + '/' + filename, 'r') as fin:
		frame_info = list(csv.reader(fin, delimiter=','))[0]
	with open(folder + '/' + filename, 'r') as fin:
		markers = list(csv.reader(fin, delimiter=','))[9][1:]
	with open(folder + '/' + filename, 'r') as fin:
		data = np.genfromtxt(fin, delimiter=',', skip_header=11)
		
	data_out = np.zeros((len(data), 2+3*6))
	data_out[:,0] = np.int_(data[:,0])
	data_out[:,1] = data[:,1]

	label_names = ['Frame#', 'Time']
	column_names = ['', '']
	i = 0
	for m in marker_names[subject]:
		for j in range(3):
			data_out[:,2+3*i+j] = data[:,2+3*markers.index(m)+j]
		i += 1
		label_names.append(m)
		label_names.append('')
		label_names.append('')
		column_names.append('X' + str(i))	
		column_names.append('Y' + str(i))
		column_names.append('Z' + str(i))
		
	l1 = ['PathFileType', '', '4', '(X/Y/Z)', 'C:\Documents and Settings']
	l2 = ['DataRate', 'CameraRate', 'NumFrames', 'NumMarkers', 'Units', 'OrigDataRate', 'OrigDataStartFrame', 'OrigNumFrames']
	l3 = [150, 150, frame_info[1], len(marker_names[subject]), 'mm', 150, 1, frame_info[1]]
	l4 = label_names
	l5 = column_names
	
	for l in [l1, l2, l3, l4, l5]:
		while len(l) < 2 + 3*len(marker_names[subject]):
			l.append('')
			
	fileout = filename.split('.')[0] + '_' + subject + '.trc'
	with open('IK_data/' + fileout, 'w') as fout:
		writer = csv.writer(fout, delimiter="\t")
		writer.writerows([l1, l2, l3, l4, l5])
		writer.writerow([])
		
		for i in range(len(data_out)):
			line = [str(i)]
			for j in range(1, len(data_out[i])):
				line.append(data_out[i,j])
			writer.writerow(line)
		