import opensim as ops
import os
import csv
import numpy as np


for d in range(2,9):
	dyad = str(d)
	for subject in ['S1', 'S2']:
	
		## Define filenames
		model_filename = 'ArmModel_' + subject + '.osim'
		scaled_model_filename = 'ArmModel_scaled.osim'
		adjusted_model_filename = 'ArmModel_adjusted.osim'
		scale_setup_filename = 'scale_setup_' + subject + '.xml'
		IK_setup_filename = 'IK_setup_' + subject + '.xml'
		mocap_folder = 'IK_data/Dyad_' + dyad
		output_folder = 'IK_output/Dyad_' + dyad
		marker_placement_file = 'Dyad_' + dyad + '_' + subject + 'MarkerPlacement.csv'

		markers = {'S1': ['s1Shoulder', 's1Elbow', 's1Hand1', 's1Hand2', 's1Hand3', 's1Hand4'], 'S2' : ['s2Shoulder', 's2Elbow', 's2Hand1', 's2Hand2', 's2Hand3', 's2Hand4']}
		torso_rotation = {'S1': {'r_x' : np.pi/2., 'r_y' : 0., 'r_z' : 0.}, 'S2' : {'r_x' : np.pi/2., 'r_y' : np.pi, 'r_z' : 0.}}

		with open(marker_placement_file, 'w') as fout:
			writer = csv.writer(fout, delimiter = ',')
			column_header = ['Dyad', 'Trial']
			for m in markers[subject]:
				column_header.append(m + ' X')
				column_header.append(m + ' Y')
				column_header.append(m + ' Z')
			writer.writerow(column_header)

		for f in sorted(os.listdir(mocap_folder)):
			if f.split('_')[-1].split('.')[0] == subject:
				mocap_filename = mocap_folder + '/' + f
				IK_output_filename = output_folder + '/' + f.split('Kinematic')[0] + 'IK_' + subject + '.mot'
				
				## Load initial model
				model = ops.Model(model_filename)
				state = model.initSystem()

				# Lock torso rotation
				for coord in ['r_x', 'r_y', 'r_z']:
					c = model.getCoordinateSet().get(coord)
					# c.set_locked(0)
					# c.set_default_value(torso_rotation[subject][coord])
					c.set_locked(1)

				## Create scale tool
				scaleTool = ops.ScaleTool(scale_setup_filename)
				
				# Get trc data to determine time range
				markerData = ops.MarkerData(mocap_filename)
				initial_scale_time = markerData.getStartFrameTime()
				final_scale_time = markerData.getLastFrameTime()
				timeArray = ops.ArrayDouble()
				timeArray.set(0,initial_scale_time)
				timeArray.set(1,final_scale_time) 

				# Tell scale tool to use the loaded model
				scaleTool.getGenericModelMaker().setModelFileName(model_filename)

				## ModelScaler-
				# Whether or not to use the model scaler during scale
				scaleTool.getModelScaler().setApply(1)
				# Set the marker file (.trc) to be used for scaling 
				scaleTool.getModelScaler().setMarkerFileName(mocap_filename)
				# set a time range
				scaleTool.getModelScaler().setTimeRange(timeArray)
				# Indicating whether or not to preserve relative mass between segments
				scaleTool.getModelScaler().setPreserveMassDist(0)
				#Name of OpenSim model file (.osim) to write when done scaling.
				scaleTool.getModelScaler().setOutputModelFileName(scaled_model_filename)
				# Run model scaler Tool
				scaleTool.getModelScaler().processModel(state, model, scaleTool.getPathToSubject())

				# Update state to current state
				state = model.getWorkingState()

				## ModelPlacer-
				# Whether or not to use the model scaler during scale
				scaleTool.getMarkerPlacer().setApply(1)
				# Set the marker placer time range
				scaleTool.getMarkerPlacer().setTimeRange(timeArray)
				# Set the marker file (.trc) to be used for scaling 
				scaleTool.getMarkerPlacer().setStaticPoseFileName(mocap_filename)
				scaleTool.getMarkerPlacer().setOutputModelFileName(adjusted_model_filename)
				# Maximum amount of movement allowed in marker data when averaging
				scaleTool.getMarkerPlacer().setMaxMarkerMovement(-1)
				# Run Marker Placer
				scaleTool.getMarkerPlacer().processModel(state, model, scaleTool.getPathToSubject())

				# Update state to current state
				state = model.getWorkingState()
				
				with open(marker_placement_file, 'a') as fout:
					writer = csv.writer(fout, delimiter = ',')
					data_marker = [f.split('_')[1], f.split('_')[5]]
					for m in markers[subject]:
						for j in range(3):
							data_marker.append(model.getMarkerSet().get(m).getOffset().get(j))
					writer.writerow(data_marker)

				# Lock torso displacements
				for coord in ['r_x', 'r_y', 'r_z', 't_x', 't_y', 't_z']:
					c = model.getCoordinateSet().get(coord)
					c.set_default_value(c.getValue(state))
					c.set_locked(1)

				## Create IK tool
				IK_tool = ops.InverseKinematicsTool(IK_setup_filename)
				IK_tool.setModel(model)

				#Get initial and final time 
				IK_tool.setStartTime(markerData.getStartFrameTime())
				IK_tool.setEndTime(markerData.getLastFrameTime())

				IK_tool.setMarkerDataFileName(mocap_filename)
				IK_tool.setOutputMotionFileName(IK_output_filename)
				IK_tool.run()

