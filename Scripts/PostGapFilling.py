##################################################################################
# Libraries

from signal import pause
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import sys

# Cheat Part: juste to debug this programm as a standalone 
# Define subject and trial to compute
# campaign_name = 'Mover202207'
# subject = '1543'
# trial = '0001'
# sys.argv.append(campaign_name)
# sys.argv.append(subject)
# sys.argv.append(int(trial))

input_file = ""

# Listen .sh parameters
if(len(sys.argv) == 1):
    print("Please provide the campaign name, the subject name and the trial")
    exit()
elif(len(sys.argv) == 2):
    print("Please provide the subject name and the trial")
    exit()
elif(len(sys.argv) == 3):
    print("Please provide the trial")
    exit()
else:
    campaign_name = sys.argv[1]
    subject = sys.argv[2]
    trial = int(sys.argv[3])
    trial_str = f'{trial:04d}'


##################################################################################
## Files
# Input file
input_mocap_filename = '/home/raphael/Documents/MOVER/Data/Mover202207/Qualisys/'+subject+'/'+trial_str+'_full.trc'

# Output files
output_mocap_filename_clean = '/home/raphael/Documents/MOVER/Data/Mover202207/Qualisys/'+subject+'/'+trial_str+'_full_clean.trc'
output_mocap_filename_force_filling = '/home/raphael/Documents/MOVER/Data/Mover202207/Qualisys/'+subject+'/'+trial_str+'_full_forced.trc'



##################################################################################
# # In the experience MOVER n°1 (August 2022), the trial n°1 of the subject n°1543 shows a precision to the [cm] in the joint center reconstruction.
# Let's consider this precision for our computation.
# Though, we determine the maximum gap time to fill according to this precision and the theorical velocity of the movement (given by the handle center speed)
v_human = 0.025 #[m/s]
t_max_filling = 0.010 / v_human
print("This programm will fill gaps of less than ", t_max_filling,' seconds')
# Second phase: fill huge gaps:
print("Activation of the second phase")
# input("WARNING: you're about to fill huge gaps. Press anything to continue...")
print("you're about to fill huge gaps.")
t_max_filling_forced = 100000 # equivalent to unlimited in the case of the experiment
print("This programm will fill gaps in the middle of the trajectory")

##################################################################################
# Open .trc file from Qualisys

## Extract Data from the .trc file and split them in multiple dataframes
print("\nExtracting Data from .trc file: ", input_mocap_filename)
# Extract first row of the .trc file
print('- Extract 1st line')
header_l1_trc = pd.read_csv(input_mocap_filename, nrows=1, header = None, delimiter='\t')

# Extract parameter header
print('- Extract Parameters')
parameters = pd.read_csv(input_mocap_filename, nrows=1, header = 1, skiprows=0, delimiter='\t')

# if data in .trc file are expressed in [mm], we have to convert joints center also in [mm] (multiply by 1000): the data from OSimModel.py are expressed in [m]
unit_multiplier = 1 
if parameters['Units'][0] =='mm': 
    unit_multiplier = 1000
    print('\tWARNING: Data in .trc file are expressed in [mm]')

# Extract markers names of the .trc file
print('- Extract markers names')
markers_header = pd.read_csv(input_mocap_filename, nrows=1 , header = None ,skiprows=3) ## make just one celle tab --> use it only for export
markers_trc = pd.read_csv(input_mocap_filename, nrows=2 , header = None , delimiter='\t',skiprows=3) # multi cells tab, but contains NaN values
print("markers names\n", markers_trc)
# Avoid NaN in the markers names dataframe, by replacinf NaN with the marker name
for col_idx in range(markers_trc.shape[1]-1):
    if pd.isna(markers_trc.iloc[0,col_idx]) and pd.isna(markers_trc.iloc[0,col_idx+1]):
        markers_trc.iloc[0,col_idx] = markers_trc.iloc[0,col_idx + 2]
        markers_trc.iloc[0,col_idx+1] = markers_trc.iloc[0,col_idx + 2]

print("markers names\n", markers_trc)
# Extract dataframe containing coordinates of the markers
print('- Extract data')
data_trc = pd.read_csv(input_mocap_filename, header = 1, delimiter='\t',skiprows=3)

##################################################################################
# Compute
# print data dataframe

print("Data:\n",data_trc)

print("Copying dataframe")
data_trc_filled_clean = data_trc.copy()
data_trc_filled_forced = data_trc.copy()

##################################################################################
################################################################################## VALIDATED
# Test part with handmade dataframe
# test_df = pd.DataFrame({'Index':[0,1,2,3,4,5,6,7,8,9,10],'Time':[0,0.001,0.002,0.003,0.004,0.005,0.006,0.007,0.008,0.009,0.010]})
# test_df['Debut']=[0,0,0,0,1.25,1.28,1.32,1.56,1.47,1.84,1.49]
# test_df['Fin']=[1.56,1.47,1.84,1.49,1.25,1.28,1.32,0,0,0,0]
# test_df['Milieu']=[1.56,1.47,1.84,0,0,0,0,1.49,1.25,1.28,1.32]
# data_trc = test_df.copy()
##################################################################################
##################################################################################

# print("Copy:\n",data_trc)

##################################################################################
# FUNCTIONS
##################################################################################

def FillGap(data_col, i0, iN):
    data_filled = data_col.copy()
    if i0 ==0 and iN == data_filled.shape[0]-1:
        input("ERROR: data is empty !")
    elif i0 == 0: # Gap at the begining of a trajectory
        input("ERROR: the gap is at the beginning of the trajectory")
    elif iN == data_filled.shape[0]-1: # Gap at the end of a trajectory
        input("ERROR: the gap is at the end of the trajectory")
    else: # Gap in the middle of a trajectory
        delta = data_filled[iN+1] - data_filled[i0 - 1] # Compute the Step to fill
        a = delta / (iN - i0 + 2)# Compute the growth rate from the step
        for i in range(i0, iN+1):
            data_filled.iloc[i] = data_filled.iloc[i0 - 1] + a * (i-i0+1)

    return data_filled

##################################################################################
# MAIN
##################################################################################

not_export_flag = False
not_export_clean_flag = False


# Gaps analyzer
print("Columns: ", data_trc.columns)
columns_to_fill = data_trc.columns[data_trc.columns!='Unnamed: 0']
columns_to_fill = columns_to_fill[columns_to_fill!='Unnamed: 1']
col_idx = 2 # the first index begins at 2 because of the removing of Unnamed 0 and Unnamed 1
print("Columns to fill : ", columns_to_fill)
for col in columns_to_fill: # Check each column one by one, if it contains 0 values (corresponds to NaN values in Qualisys)
    if not_export_flag == False:
        print("\nColumn name:",col)
        empty_data = np.where(data_trc[col]==0)
        if len(empty_data[0] > 0 ):
            print("empty_data in the column:", empty_data)
            idx_0 = empty_data[0][0]
            idx_N = idx_0
            for i in range(len(empty_data[0])):
                if (empty_data[0][i] - idx_N) == 1 : # check if the values are consecutive
                    idx_N = empty_data[0][i]
                elif (empty_data[0][i] - idx_N) > 1  : # if not consecutive values, fil the previous gap, and restart a new block
                    # Verify is this gap is fillable
                    t_to_fill = data_trc.iloc[idx_N,1] -  data_trc.iloc[idx_0,1] # 'Time' column is the 2nd column (index 1) in a .trc file
                    print("Time to fill = ",t_to_fill)
                    print("Subject n°",subject," - Trial n°", trial_str," - shape : ",data_trc.shape[0]-1)
                    if idx_0 == 0 or idx_N == data_trc.shape[0]-1: # Check if the gap is at the beginning or the end of the trajectory, and if it is, do not export the trajectory:
                        not_export_flag = True
                        input("Subject n°"+str(subject)+" - Trial n°"+trial_str+" --> Data will not be exported because of edge problems ")
                    elif t_to_fill < t_max_filling:
                        print("Filling Processing (reasonable gaps)")
                        data_trc_filled_clean[col] = FillGap(data_trc_filled_clean[col],idx_0, idx_N) # Fill the gap for the "clean" dataframe with small gaps
                        data_trc_filled_forced[col] = FillGap(data_trc_filled_forced[col],idx_0, idx_N) # Fill the gap for the "clean" dataframe with small gaps even in forced dataframe
                        print(data_trc_filled_clean[col]) # Fill the gap
                    else: 
                        print("Filling Processing (wide gaps in the middle of the trajectory)")
                        data_trc_filled_forced[col] = FillGap(data_trc_filled_forced[col],idx_0, idx_N) # Fill the gap for the "clean" dataframe with small gaps
                        print(data_trc_filled_forced[col]) # Fill the gap
                        not_export_clean_flag = True
                    idx_0 = empty_data[0][i] # restart a new block with 0 values
                    idx_N = idx_0 # restart a new block with 0 values
            # For the last element, take the block and fill it !
            # Verify is this gap is fillable
            t_to_fill = data_trc.iloc[idx_N,1] -  data_trc.iloc[idx_0,1] # 'Time' column is the 2nd column (index 1) in a .trc file
            print("Time to fill = ",t_to_fill)
            if idx_0 == 0 or idx_N == data_trc.shape[0]-1: # Check if the gap is at the beginning or the end of the trajectory, and if it is, do not export the trajectory:
                not_export_flag = True
                input("Subject n°"+str(subject)+" - Trial n°"+trial_str+" --> Data will not be exported because of edge problems ")
            elif t_to_fill < t_max_filling:
                print("Filling Processing (reasonable gaps)")
                data_trc_filled_clean[col] = FillGap(data_trc_filled_clean[col],idx_0, idx_N) # Fill the gap for the "clean" dataframe with small gaps
                data_trc_filled_forced[col] = FillGap(data_trc_filled_forced[col],idx_0, idx_N) # Fill the gap for the "clean" dataframe with small gaps even in forced dataframe
                print(data_trc_filled_clean[col]) # Fill the gap
            else: 
                print("Filling Processing (wide gaps in the middle of the trajectory)")
                data_trc_filled_forced[col] = FillGap(data_trc_filled_forced[col],idx_0, idx_N) # Fill the gap for the "clean" dataframe with small gaps
                print(data_trc_filled_forced[col]) # Fill the gap
                not_export_clean_flag = True
        else: 
            print("No gap to fill")       
        col_idx += 1


##################################################################################
# Save new .trc file if  not_export_flag == False
if not_export_flag == False:

    # Export Forced dataframe
    # Replace Dirty columns names by null string ''
    data_trc_filled_forced.rename(columns={'Unnamed: 0':'','Unnamed: 1':''}, inplace=True) 

    ## Reconsitute the .trc file
    # Concatenate in the right order the 4 dataframes to obtain the .trc file like the original one
    print('\nSaving the new .trc file...')
    header_l1_trc.to_csv(output_mocap_filename_force_filling, index=False, sep='\t', header=False)
    parameters.to_csv(output_mocap_filename_force_filling, mode="a", index=False, sep='\t', header=True) # mode "a" means to append dataframe in the .csv file

    #  Fill the header markers names
    markers_header.to_csv(output_mocap_filename_force_filling, mode="a", index=False, sep=' ', header=False) # a simple ' ' [space] string works perfectly to transform the 1 cell dataframe into a multi-cells csv file (the \t are kept in the one cell df)
    
    # Fill the dataframe with new values
    data_trc_filled_forced.to_csv(output_mocap_filename_force_filling, mode="a", index=False, sep='\t', header=True)

    print('\t-> Data FORCED saved in ',output_mocap_filename_force_filling)

    # Export Clean dataframe if not_export_clean_flag is False
    if not_export_clean_flag == False:
        # Replace Dirty columns names by null string ''
        data_trc_filled_clean.rename(columns={'Unnamed: 0':'','Unnamed: 1':''}, inplace=True) 

        ## Reconsitute the .trc file
        # Concatenate in the right order the 4 dataframes to obtain the .trc file like the original one
        print('\nSaving the new .trc file...')
        header_l1_trc.to_csv(output_mocap_filename_clean, index=False, sep='\t', header=False)
        parameters.to_csv(output_mocap_filename_clean, mode="a", index=False, sep='\t', header=True) # mode "a" means to append dataframe in the .csv file

        #  Fill the header markers names
        markers_header.to_csv(output_mocap_filename_clean, mode="a", index=False, sep=' ', header=False) # a simple ' ' [space] string works perfectly to transform the 1 cell dataframe into a multi-cells csv file (the \t are kept in the one cell df)
        
        # Fill the dataframe with new values
        data_trc_filled_clean.to_csv(output_mocap_filename_clean, mode="a", index=False, sep='\t', header=True)
        print('\t-> Data CLEAN saved in ',output_mocap_filename_clean)
else:
    input("Subject n°"+str(subject)+" - Trial n°"+trial_str+" --> Data not exported because of edge problems ")



