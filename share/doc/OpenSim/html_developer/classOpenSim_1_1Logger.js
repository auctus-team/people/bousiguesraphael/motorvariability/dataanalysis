var classOpenSim_1_1Logger =
[
    [ "Level", "classOpenSim_1_1Logger.html#ab9bf77d271bae9aa6b904d4b71a0038b", [
      [ "Off", "classOpenSim_1_1Logger.html#ab9bf77d271bae9aa6b904d4b71a0038bad15305d7a4e34e02489c74a5ef542f36", null ],
      [ "Critical", "classOpenSim_1_1Logger.html#ab9bf77d271bae9aa6b904d4b71a0038ba278d01e5af56273bae1bb99a98b370cd", null ],
      [ "Error", "classOpenSim_1_1Logger.html#ab9bf77d271bae9aa6b904d4b71a0038ba902b0d55fddef6f8d651fe1035b7d4bd", null ],
      [ "Warn", "classOpenSim_1_1Logger.html#ab9bf77d271bae9aa6b904d4b71a0038ba56525ae64d370c0b448ac0d60710ef17", null ],
      [ "Info", "classOpenSim_1_1Logger.html#ab9bf77d271bae9aa6b904d4b71a0038ba4059b0251f66a18cb56f544728796875", null ],
      [ "Debug", "classOpenSim_1_1Logger.html#ab9bf77d271bae9aa6b904d4b71a0038baa603905470e2a5b8c13e96b579ef0dba", null ],
      [ "Trace", "classOpenSim_1_1Logger.html#ab9bf77d271bae9aa6b904d4b71a0038badd4ec0ac4e58f7c32a01244ae91150b1", null ]
    ] ],
    [ "Logger", "classOpenSim_1_1Logger.html#a3dcb06837a4266a621f7609c7ae9f063", null ],
    [ "log_cout", "classOpenSim_1_1Logger.html#a4f653d7d131bf90fe00c293b9cfd9d26", null ],
    [ "log_critical", "classOpenSim_1_1Logger.html#ad5a89a3ecbe0b1a88eae7dccf73af2b0", null ],
    [ "log_debug", "classOpenSim_1_1Logger.html#ae68b6746c2a1650da00a270e54334bd3", null ],
    [ "log_error", "classOpenSim_1_1Logger.html#ab56a381bdaf340f6bc58ab73a67de4a8", null ],
    [ "log_info", "classOpenSim_1_1Logger.html#ac4d15dcdc1d233ba73afca5adfcc7cd2", null ],
    [ "log_trace", "classOpenSim_1_1Logger.html#ae48973bbd459ae7f2e3f3644a798028e", null ],
    [ "log_warn", "classOpenSim_1_1Logger.html#a83e8a51160dc02d15543381e38405651", null ]
];