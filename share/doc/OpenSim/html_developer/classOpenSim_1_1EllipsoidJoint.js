var classOpenSim_1_1EllipsoidJoint =
[
    [ "Self", "classOpenSim_1_1EllipsoidJoint.html#af398bfe974fb3da5f5fada4bc3624599", null ],
    [ "Super", "classOpenSim_1_1EllipsoidJoint.html#aecce177303aa203bd279ad75a8030467", null ],
    [ "Coord", "classOpenSim_1_1EllipsoidJoint.html#ae4cd9fa32c06d8811137bdead0ec38f3", [
      [ "Rotation1X", "classOpenSim_1_1EllipsoidJoint.html#ae4cd9fa32c06d8811137bdead0ec38f3a8a46031bead7d82e8f009fabe55cb2e4", null ],
      [ "Rotation2Y", "classOpenSim_1_1EllipsoidJoint.html#ae4cd9fa32c06d8811137bdead0ec38f3ad4e0c8d1378bf0690919c7fc0d17fcd7", null ],
      [ "Rotation3Z", "classOpenSim_1_1EllipsoidJoint.html#ae4cd9fa32c06d8811137bdead0ec38f3a9839309ced6214b8d9a5c5ef7c7db198", null ]
    ] ],
    [ "assign", "classOpenSim_1_1EllipsoidJoint.html#a933acc279c2095c9b5f44c6b2a0e5feb", null ],
    [ "clone", "classOpenSim_1_1EllipsoidJoint.html#aceaa89e750daa5468ff20ea1d12a5ede", null ],
    [ "getConcreteClassName", "classOpenSim_1_1EllipsoidJoint.html#aebe945c7cb13d4f8667ece8c0419739e", null ]
];